<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTMatchPitchingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_match_pitchings', function (Blueprint $table) {
            $table->increments('match_pitching_id')->comment('投球ID');
            $table->unsignedInteger('match_id')->comment('試合ID');
            $table->unsignedTinyInteger('pitching_order')->nullable()->comment('投球順');
            $table->unsignedInteger('player_id')->nullable()->comment('選手ID');
            $table->unsignedInteger('innings')->nullable()->comment('投球回');
            $table->unsignedInteger('fraction_innings')->nullable()->comment('投球回（端数）');
            $table->unsignedInteger('number')->nullable()->comment('打者数');
            $table->unsignedInteger('at_bat')->nullable()->comment('投球回');
            $table->unsignedInteger('hits')->nullable()->comment('被安打');
            $table->unsignedInteger('homerun')->nullable()->comment('被本塁打');
            $table->unsignedInteger('strike_out')->nullable()->comment('奪三振');
            $table->unsignedInteger('walk')->nullable()->comment('与四球');
            $table->unsignedInteger('plunked')->nullable()->comment('与死球');
            $table->unsignedInteger('run')->nullable()->comment('失点');
            $table->unsignedInteger('earned_run')->nullable()->comment('自責点');
            $table->unsignedInteger('throw_full_innings')->nullable()->comment('完投フラグ');
            $table->unsignedInteger('win')->nullable()->comment('勝利');
            $table->unsignedInteger('lose')->nullable()->comment('敗戦');
            $table->unsignedInteger('hold')->nullable()->comment('ホールド');
            $table->unsignedInteger('save')->nullable()->comment('セーブ');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'))->comment('作成日時');
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'))->comment('更新日時');
            $table->unsignedTinyInteger('delete_flag')->nullable()->comment('削除フラグ 1:論理削除');

            $table->foreign('match_id')
                    ->references('match_id')
                    ->on('t_match_masters')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            $table->foreign('player_id')
                    ->references('player_id')
                    ->on('m_players')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_match_pitchings');
    }
}
