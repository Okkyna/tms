<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMPlayersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_players', function (Blueprint $table) {
            $table->increments('player_id')->comment('選手ID');
            $table->string('name',256)->comment('登録名');
            $table->string('number',256)->nullable()->comment('背番号');
            $table->unsignedInteger('sort_number')->comment('並び順');
            $table->text('role')->nullable()->comment('役割');
            $table->unsignedTinyInteger('player_category')->nullable()->comment('選手区分 1:現役選手 2:マネージャ／スタッフ 9:引退選手');
            $table->text('acquired_titles')->nullable()->comment('獲得タイトル');
            $table->text('copy')->nullable()->comment('選手キャッチコピー');
            $table->mediumText('text')->nullable()->comment('選手紹介文');
            $table->mediumText('author_text')->nullable()->comment('戦評・スコア記述者用説明文');
            $table->string('photo_main',256)->nullable()->comment('サムネイル／メイン写真');
            $table->string('photo_1',256)->nullable()->comment('写真1');
            $table->string('photo_2',256)->nullable()->comment('写真2');
            $table->string('photo_3',256)->nullable()->comment('写真3');
            $table->string('photo_4',256)->nullable()->comment('写真4');
            $table->string('photo_5',256)->nullable()->comment('写真5');
            $table->date('join_at')->comment('入団年月');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'))->comment('作成日時');
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'))->comment('更新日時');
            $table->unsignedTinyInteger('delete_flag')->nullable()->comment('削除フラグ 1:論理削除');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_players');
    }
}
