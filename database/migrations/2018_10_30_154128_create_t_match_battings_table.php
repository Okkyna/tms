<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTMatchBattingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_match_battings', function (Blueprint $table) {
            $table->increments('match_batting_id')->comment('打撃ID');
            $table->unsignedInteger('match_id')->comment('試合ID');
            $table->unsignedTinyInteger('batting_order')->comment('打順');
            $table->unsignedInteger('player_id')->nullable()->comment('選手ID');
            $table->string('position',256)->nullable()->comment('守備位置');
            $table->unsignedInteger('at_bat')->nullable()->comment('打席');
            $table->unsignedInteger('at_stroke')->nullable()->comment('打数');
            $table->unsignedInteger('hits')->nullable()->comment('安打');
            $table->unsignedInteger('doubles')->nullable()->comment('二塁打');
            $table->unsignedInteger('triples')->nullable()->comment('三塁打');
            $table->unsignedInteger('homerun')->nullable()->comment('本塁打');
            $table->unsignedInteger('rbi')->nullable()->comment('打点');
            $table->unsignedInteger('run')->nullable()->comment('得点');
            $table->unsignedInteger('steal')->nullable()->comment('盗塁');
            $table->unsignedInteger('walk')->nullable()->comment('四球');
            $table->unsignedInteger('plunked')->nullable()->comment('死球');
            $table->unsignedInteger('strike_out')->nullable()->comment('三振');
            $table->unsignedInteger('sacrifice_bunt')->nullable()->comment('犠打');
            $table->unsignedInteger('sacrifice_fly')->nullable()->comment('犠飛');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'))->comment('作成日時');
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'))->comment('更新日時');
            $table->unsignedTinyInteger('delete_flag')->nullable()->comment('削除フラグ 1:論理削除');

            $table->foreign('match_id')
                    ->references('match_id')
                    ->on('t_match_masters')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            $table->foreign('player_id')
                    ->references('player_id')
                    ->on('m_players')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_match_battings');
    }
}
