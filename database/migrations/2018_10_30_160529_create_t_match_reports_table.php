<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTMatchReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_match_reports', function (Blueprint $table) {
            $table->increments('match_report_id')->comment('試合レポートID');
            $table->unsignedInteger('match_id')->comment('試合ID');
            $table->mediumText('digest')->nullable()->comment('ダイジェスト');
            $table->mediumText('locker_room')->nullable()->comment('ロッカールーム');
            $table->unsignedInteger('author_id')->nullable()->comment('戦評記述者ID');
            $table->string('photo_1',256)->nullable()->comment('写真1');
            $table->string('photo_2',256)->nullable()->comment('写真2');
            $table->string('photo_3',256)->nullable()->comment('写真3');
            $table->string('photo_4',256)->nullable()->comment('写真4');
            $table->string('photo_5',256)->nullable()->comment('写真5');
            $table->string('movie_1',256)->nullable()->comment('動画1');
            $table->string('movie_2',256)->nullable()->comment('動画2');
            $table->string('movie_3',256)->nullable()->comment('動画3');
            $table->string('movie_4',256)->nullable()->comment('動画4');
            $table->string('movie_5',256)->nullable()->comment('動画5');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'))->comment('作成日時');
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'))->comment('更新日時');
            $table->unsignedTinyInteger('delete_flag')->nullable()->comment('削除フラグ 1:論理削除');

            $table->foreign('match_id')
                    ->references('match_id')
                    ->on('t_match_masters')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            $table->foreign('author_id')
                    ->references('player_id')
                    ->on('m_players')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_match_reports');
    }
}
