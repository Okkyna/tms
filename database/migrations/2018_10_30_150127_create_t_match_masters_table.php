<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTMatchMastersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_match_masters', function (Blueprint $table) {
            $table->unsignedInteger('match_id')->comment('試合ID');
            $table->unsignedTinyInteger('match_category')->nullable()->comment('試合区分 100:練習試合 201:スカイカップ');
            $table->date('date')->comment('試合実施（予定）日');
            $table->time('start_time')->nullable()->comment('開始時間');
            $table->time('end_time')->nullable()->comment('終了時間');
            $table->unsignedInteger('ground_id')->nullable()->comment('グランドID');
            $table->string('opponent',256)->nullable()->comment('対戦相手チーム名');
            $table->unsignedTinyInteger('result')->nullable()->comment('結果区分 1:勝利 2:引き分け 3:敗戦');
            $table->unsignedTinyInteger('match_order')->nullable()->comment('攻撃順 1:先攻 2:後攻');
            $table->unsignedInteger('front_1')->nullable()->comment('1回表');
            $table->unsignedInteger('back_1')->nullable()->comment('1回裏');
            $table->unsignedInteger('front_2')->nullable()->comment('2回表');
            $table->unsignedInteger('back_2')->nullable()->comment('2回裏');
            $table->unsignedInteger('front_3')->nullable()->comment('3回表');
            $table->unsignedInteger('back_3')->nullable()->comment('3回裏');
            $table->unsignedInteger('front_4')->nullable()->comment('4回表');
            $table->unsignedInteger('back_4')->nullable()->comment('4回裏');
            $table->unsignedInteger('front_5')->nullable()->comment('5回表');
            $table->unsignedInteger('back_5')->nullable()->comment('5回裏');
            $table->unsignedInteger('front_6')->nullable()->comment('6回表');
            $table->unsignedInteger('back_6')->nullable()->comment('6回裏');
            $table->unsignedInteger('front_7')->nullable()->comment('7回表');
            $table->unsignedInteger('back_7')->nullable()->comment('7回裏');
            $table->unsignedInteger('front_8')->nullable()->comment('8回表');
            $table->unsignedInteger('back_8')->nullable()->comment('8回裏');
            $table->unsignedInteger('front_9')->nullable()->comment('9回表');
            $table->unsignedInteger('back_9')->nullable()->comment('9回裏');
            $table->unsignedInteger('first_total')->nullable()->comment('先攻合計');
            $table->unsignedInteger('after_total')->nullable()->comment('後攻合計');
            $table->unsignedInteger('scorer_id')->nullable()->comment('スコアラーID');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'))->comment('作成日時');
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'))->comment('更新日時');
            $table->unsignedTinyInteger('delete_flag')->nullable()->comment('削除フラグ 1:論理削除');

            $table->primary(['match_id']);

            $table->foreign('ground_id')
                    ->references('ground_id')
                    ->on('m_grounds')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');

            $table->foreign('scorer_id')
                    ->references('player_id')
                    ->on('m_players')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_match_masters');
    }
}
