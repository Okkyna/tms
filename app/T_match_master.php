<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class T_match_master extends Model
{
    protected $primaryKey = 'match_id';
    public $incrementing = false;

    public function m_grounds()
    {
        return $this->belongsTo('App\M_ground');
    }

}
