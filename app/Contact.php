<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = [
        'type', 'name', 'email', 'body'
    ];
    static $types = [
        '入団に関する問い合わせ', '対戦に関する問い合わせ', 'その他'
    ];
}
