<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\M_player;
use App\T_match_batting;
use App\T_match_pitching;
use DB;
use Carbon\Carbon;


class RestplayerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $members = DB::table('m_players')
                    ->select('*')
                    ->whereNull('delete_flag')
                    ->whereIn('player_category',[1, 2])
                    ->orderBy('sort_number','asc')
                    ->get();
        return $members->toArray();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($player_id)
    {
        $member = M_player::findOrFail($player_id);
        $batting_data_all = T_match_batting::select(DB::raw('count(match_id) as games'), DB::raw("sum(at_bat) as at_bat, sum(at_stroke) as at_stroke, sum(hits) as hits, sum(doubles) as doubles, sum(triples) as triples, sum(homerun) as homerun, round(sum(hits)/sum(at_stroke), 3) as AVG, sum(rbi) as rbi, sum(run) as run, sum(steal) as steal, sum(walk) as walk, sum(plunked) as plunked, sum(strike_out) as strike_out, sum(sacrifice_bunt) as sacrifice_bunt, sum(sacrifice_fly) as sacrifice_fly, round((sum(hits)+sum(walk)+sum(plunked))/(sum(at_stroke)+sum(walk)+sum(plunked)+sum(sacrifice_fly)), 3) as OBP, round((sum(hits)-sum(doubles)-sum(triples)-sum(homerun)+sum(doubles)*2+sum(triples)*3+sum(homerun)*4)/sum(at_stroke), 3) as SLG, round((sum(hits)+sum(walk)+sum(plunked))/(sum(at_stroke)+sum(walk)+sum(plunked)+sum(sacrifice_fly))+(sum(hits)-sum(doubles)-sum(triples)-sum(homerun)+sum(doubles)*2+sum(triples)*3+sum(homerun)*4)/sum(at_stroke), 3) as OPS"))
                            ->where("player_id", "=", "$player_id")
                            ->whereNull('delete_flag')
                            ->get();
        $batting_data_2021 = T_match_batting::select(DB::raw('count(match_id) as games'), DB::raw("sum(at_bat) as at_bat, sum(at_stroke) as at_stroke, sum(hits) as hits, sum(doubles) as doubles, sum(triples) as triples, sum(homerun) as homerun, round(sum(hits)/sum(at_stroke), 3) as AVG, sum(rbi) as rbi, sum(run) as run, sum(steal) as steal, sum(walk) as walk, sum(plunked) as plunked, sum(strike_out) as strike_out, sum(sacrifice_bunt) as sacrifice_bunt, sum(sacrifice_fly) as sacrifice_fly, round((sum(hits)+sum(walk)+sum(plunked))/(sum(at_stroke)+sum(walk)+sum(plunked)+sum(sacrifice_fly)), 3) as OBP, round((sum(hits)-sum(doubles)-sum(triples)-sum(homerun)+sum(doubles)*2+sum(triples)*3+sum(homerun)*4)/sum(at_stroke), 3) as SLG, round((sum(hits)+sum(walk)+sum(plunked))/(sum(at_stroke)+sum(walk)+sum(plunked)+sum(sacrifice_fly))+(sum(hits)-sum(doubles)-sum(triples)-sum(homerun)+sum(doubles)*2+sum(triples)*3+sum(homerun)*4)/sum(at_stroke), 3) as OPS"))
                            ->where("player_id", "=", "$player_id")
                            ->whereNull('delete_flag')
                            ->whereBetween("match_id", [2021000000, 2021999999])
                            ->get();
        $batting_data_2020 = T_match_batting::select(DB::raw('count(match_id) as games'), DB::raw("sum(at_bat) as at_bat, sum(at_stroke) as at_stroke, sum(hits) as hits, sum(doubles) as doubles, sum(triples) as triples, sum(homerun) as homerun, round(sum(hits)/sum(at_stroke), 3) as AVG, sum(rbi) as rbi, sum(run) as run, sum(steal) as steal, sum(walk) as walk, sum(plunked) as plunked, sum(strike_out) as strike_out, sum(sacrifice_bunt) as sacrifice_bunt, sum(sacrifice_fly) as sacrifice_fly, round((sum(hits)+sum(walk)+sum(plunked))/(sum(at_stroke)+sum(walk)+sum(plunked)+sum(sacrifice_fly)), 3) as OBP, round((sum(hits)-sum(doubles)-sum(triples)-sum(homerun)+sum(doubles)*2+sum(triples)*3+sum(homerun)*4)/sum(at_stroke), 3) as SLG, round((sum(hits)+sum(walk)+sum(plunked))/(sum(at_stroke)+sum(walk)+sum(plunked)+sum(sacrifice_fly))+(sum(hits)-sum(doubles)-sum(triples)-sum(homerun)+sum(doubles)*2+sum(triples)*3+sum(homerun)*4)/sum(at_stroke), 3) as OPS"))
                            ->where("player_id", "=", "$player_id")
                            ->whereNull('delete_flag')
                            ->whereBetween("match_id", [2020000000, 2020999999])
                            ->get();
        $batting_data_2019 = T_match_batting::select(DB::raw('count(match_id) as games'), DB::raw("sum(at_bat) as at_bat, sum(at_stroke) as at_stroke, sum(hits) as hits, sum(doubles) as doubles, sum(triples) as triples, sum(homerun) as homerun, round(sum(hits)/sum(at_stroke), 3) as AVG, sum(rbi) as rbi, sum(run) as run, sum(steal) as steal, sum(walk) as walk, sum(plunked) as plunked, sum(strike_out) as strike_out, sum(sacrifice_bunt) as sacrifice_bunt, sum(sacrifice_fly) as sacrifice_fly, round((sum(hits)+sum(walk)+sum(plunked))/(sum(at_stroke)+sum(walk)+sum(plunked)+sum(sacrifice_fly)), 3) as OBP, round((sum(hits)-sum(doubles)-sum(triples)-sum(homerun)+sum(doubles)*2+sum(triples)*3+sum(homerun)*4)/sum(at_stroke), 3) as SLG, round((sum(hits)+sum(walk)+sum(plunked))/(sum(at_stroke)+sum(walk)+sum(plunked)+sum(sacrifice_fly))+(sum(hits)-sum(doubles)-sum(triples)-sum(homerun)+sum(doubles)*2+sum(triples)*3+sum(homerun)*4)/sum(at_stroke), 3) as OPS"))
                            ->where("player_id", "=", "$player_id")
                            ->whereNull('delete_flag')
                            ->whereBetween("match_id", [2019000000, 2019999999])
                            ->get();
        $batting_data_2018 = T_match_batting::select(DB::raw('count(match_id) as games'), DB::raw("sum(at_bat) as at_bat, sum(at_stroke) as at_stroke, sum(hits) as hits, sum(doubles) as doubles, sum(triples) as triples, sum(homerun) as homerun, round(sum(hits)/sum(at_stroke), 3) as AVG, sum(rbi) as rbi, sum(run) as run, sum(steal) as steal, sum(walk) as walk, sum(plunked) as plunked, sum(strike_out) as strike_out, sum(sacrifice_bunt) as sacrifice_bunt, sum(sacrifice_fly) as sacrifice_fly, round((sum(hits)+sum(walk)+sum(plunked))/(sum(at_stroke)+sum(walk)+sum(plunked)+sum(sacrifice_fly)), 3) as OBP, round((sum(hits)-sum(doubles)-sum(triples)-sum(homerun)+sum(doubles)*2+sum(triples)*3+sum(homerun)*4)/sum(at_stroke), 3) as SLG, round((sum(hits)+sum(walk)+sum(plunked))/(sum(at_stroke)+sum(walk)+sum(plunked)+sum(sacrifice_fly))+(sum(hits)-sum(doubles)-sum(triples)-sum(homerun)+sum(doubles)*2+sum(triples)*3+sum(homerun)*4)/sum(at_stroke), 3) as OPS"))
                            ->where("player_id", "=", "$player_id")
                            ->whereNull('delete_flag')
                            ->whereBetween("match_id", [2018000000, 2018999999])
                            ->get();
        $batting_data_2017 = T_match_batting::select(DB::raw('count(match_id) as games'), DB::raw("sum(at_bat) as at_bat, sum(at_stroke) as at_stroke, sum(hits) as hits, sum(doubles) as doubles, sum(triples) as triples, sum(homerun) as homerun, round(sum(hits)/sum(at_stroke), 3) as AVG, sum(rbi) as rbi, sum(run) as run, sum(steal) as steal, sum(walk) as walk, sum(plunked) as plunked, sum(strike_out) as strike_out, sum(sacrifice_bunt) as sacrifice_bunt, sum(sacrifice_fly) as sacrifice_fly, round((sum(hits)+sum(walk)+sum(plunked))/(sum(at_stroke)+sum(walk)+sum(plunked)+sum(sacrifice_fly)), 3) as OBP, round((sum(hits)-sum(doubles)-sum(triples)-sum(homerun)+sum(doubles)*2+sum(triples)*3+sum(homerun)*4)/sum(at_stroke), 3) as SLG, round((sum(hits)+sum(walk)+sum(plunked))/(sum(at_stroke)+sum(walk)+sum(plunked)+sum(sacrifice_fly))+(sum(hits)-sum(doubles)-sum(triples)-sum(homerun)+sum(doubles)*2+sum(triples)*3+sum(homerun)*4)/sum(at_stroke), 3) as OPS"))
                            ->where("player_id", "=", "$player_id")
                            ->whereNull('delete_flag')
                            ->whereBetween("match_id", [2017000000, 2017999999])
                            ->get();
        $batting_data_2016 = T_match_batting::select(DB::raw('count(match_id) as games'), DB::raw("sum(at_bat) as at_bat, sum(at_stroke) as at_stroke, sum(hits) as hits, sum(doubles) as doubles, sum(triples) as triples, sum(homerun) as homerun, round(sum(hits)/sum(at_stroke), 3) as AVG, sum(rbi) as rbi, sum(run) as run, sum(steal) as steal, sum(walk) as walk, sum(plunked) as plunked, sum(strike_out) as strike_out, sum(sacrifice_bunt) as sacrifice_bunt, sum(sacrifice_fly) as sacrifice_fly, round((sum(hits)+sum(walk)+sum(plunked))/(sum(at_stroke)+sum(walk)+sum(plunked)+sum(sacrifice_fly)), 3) as OBP, round((sum(hits)-sum(doubles)-sum(triples)-sum(homerun)+sum(doubles)*2+sum(triples)*3+sum(homerun)*4)/sum(at_stroke), 3) as SLG, round((sum(hits)+sum(walk)+sum(plunked))/(sum(at_stroke)+sum(walk)+sum(plunked)+sum(sacrifice_fly))+(sum(hits)-sum(doubles)-sum(triples)-sum(homerun)+sum(doubles)*2+sum(triples)*3+sum(homerun)*4)/sum(at_stroke), 3) as OPS"))
                            ->where("player_id", "=", "$player_id")
                            ->whereNull('delete_flag')
                            ->whereBetween("match_id", [2016000000, 2016999999])
                            ->get();
        $batting_data_2015 = T_match_batting::select(DB::raw('count(match_id) as games'), DB::raw("sum(at_bat) as at_bat, sum(at_stroke) as at_stroke, sum(hits) as hits, sum(doubles) as doubles, sum(triples) as triples, sum(homerun) as homerun, round(sum(hits)/sum(at_stroke), 3) as AVG, sum(rbi) as rbi, sum(run) as run, sum(steal) as steal, sum(walk) as walk, sum(plunked) as plunked, sum(strike_out) as strike_out, sum(sacrifice_bunt) as sacrifice_bunt, sum(sacrifice_fly) as sacrifice_fly, round((sum(hits)+sum(walk)+sum(plunked))/(sum(at_stroke)+sum(walk)+sum(plunked)+sum(sacrifice_fly)), 3) as OBP, round((sum(hits)-sum(doubles)-sum(triples)-sum(homerun)+sum(doubles)*2+sum(triples)*3+sum(homerun)*4)/sum(at_stroke), 3) as SLG, round((sum(hits)+sum(walk)+sum(plunked))/(sum(at_stroke)+sum(walk)+sum(plunked)+sum(sacrifice_fly))+(sum(hits)-sum(doubles)-sum(triples)-sum(homerun)+sum(doubles)*2+sum(triples)*3+sum(homerun)*4)/sum(at_stroke), 3) as OPS"))
                            ->where("player_id", "=", "$player_id")
                            ->whereNull('delete_flag')
                            ->whereBetween("match_id", [2015000000, 2015999999])
                            ->get();
        $batting_data_2014 = T_match_batting::select(DB::raw('count(match_id) as games'), DB::raw("sum(at_bat) as at_bat, sum(at_stroke) as at_stroke, sum(hits) as hits, sum(doubles) as doubles, sum(triples) as triples, sum(homerun) as homerun, round(sum(hits)/sum(at_stroke), 3) as AVG, sum(rbi) as rbi, sum(run) as run, sum(steal) as steal, sum(walk) as walk, sum(plunked) as plunked, sum(strike_out) as strike_out, sum(sacrifice_bunt) as sacrifice_bunt, sum(sacrifice_fly) as sacrifice_fly, round((sum(hits)+sum(walk)+sum(plunked))/(sum(at_stroke)+sum(walk)+sum(plunked)+sum(sacrifice_fly)), 3) as OBP, round((sum(hits)-sum(doubles)-sum(triples)-sum(homerun)+sum(doubles)*2+sum(triples)*3+sum(homerun)*4)/sum(at_stroke), 3) as SLG, round((sum(hits)+sum(walk)+sum(plunked))/(sum(at_stroke)+sum(walk)+sum(plunked)+sum(sacrifice_fly))+(sum(hits)-sum(doubles)-sum(triples)-sum(homerun)+sum(doubles)*2+sum(triples)*3+sum(homerun)*4)/sum(at_stroke), 3) as OPS"))
                            ->where("player_id", "=", "$player_id")
                            ->whereNull('delete_flag')
                            ->whereBetween("match_id", [2014000000, 2014999999])
                            ->get();
        $batting_data_2013 = T_match_batting::select(DB::raw('count(match_id) as games'), DB::raw("sum(at_bat) as at_bat, sum(at_stroke) as at_stroke, sum(hits) as hits, sum(doubles) as doubles, sum(triples) as triples, sum(homerun) as homerun, round(sum(hits)/sum(at_stroke), 3) as AVG, sum(rbi) as rbi, sum(run) as run, sum(steal) as steal, sum(walk) as walk, sum(plunked) as plunked, sum(strike_out) as strike_out, sum(sacrifice_bunt) as sacrifice_bunt, sum(sacrifice_fly) as sacrifice_fly, round((sum(hits)+sum(walk)+sum(plunked))/(sum(at_stroke)+sum(walk)+sum(plunked)+sum(sacrifice_fly)), 3) as OBP, round((sum(hits)-sum(doubles)-sum(triples)-sum(homerun)+sum(doubles)*2+sum(triples)*3+sum(homerun)*4)/sum(at_stroke), 3) as SLG, round((sum(hits)+sum(walk)+sum(plunked))/(sum(at_stroke)+sum(walk)+sum(plunked)+sum(sacrifice_fly))+(sum(hits)-sum(doubles)-sum(triples)-sum(homerun)+sum(doubles)*2+sum(triples)*3+sum(homerun)*4)/sum(at_stroke), 3) as OPS"))
                            ->where("player_id", "=", "$player_id")
                            ->whereNull('delete_flag')
                            ->whereBetween("match_id", [2013000000, 2013999999])
                            ->get();
        $batting_data_2012 = T_match_batting::select(DB::raw('count(match_id) as games'), DB::raw("sum(at_bat) as at_bat, sum(at_stroke) as at_stroke, sum(hits) as hits, sum(doubles) as doubles, sum(triples) as triples, sum(homerun) as homerun, round(sum(hits)/sum(at_stroke), 3) as AVG, sum(rbi) as rbi, sum(run) as run, sum(steal) as steal, sum(walk) as walk, sum(plunked) as plunked, sum(strike_out) as strike_out, sum(sacrifice_bunt) as sacrifice_bunt, sum(sacrifice_fly) as sacrifice_fly, round((sum(hits)+sum(walk)+sum(plunked))/(sum(at_stroke)+sum(walk)+sum(plunked)+sum(sacrifice_fly)), 3) as OBP, round((sum(hits)-sum(doubles)-sum(triples)-sum(homerun)+sum(doubles)*2+sum(triples)*3+sum(homerun)*4)/sum(at_stroke), 3) as SLG, round((sum(hits)+sum(walk)+sum(plunked))/(sum(at_stroke)+sum(walk)+sum(plunked)+sum(sacrifice_fly))+(sum(hits)-sum(doubles)-sum(triples)-sum(homerun)+sum(doubles)*2+sum(triples)*3+sum(homerun)*4)/sum(at_stroke), 3) as OPS"))
                            ->where("player_id", "=", "$player_id")
                            ->whereNull('delete_flag')
                            ->whereBetween("match_id", [2012000000, 2012999999])
                            ->get();
        $batting_data_2011 = T_match_batting::select(DB::raw('count(match_id) as games'), DB::raw("sum(at_bat) as at_bat, sum(at_stroke) as at_stroke, sum(hits) as hits, sum(doubles) as doubles, sum(triples) as triples, sum(homerun) as homerun, round(sum(hits)/sum(at_stroke), 3) as AVG, sum(rbi) as rbi, sum(run) as run, sum(steal) as steal, sum(walk) as walk, sum(plunked) as plunked, sum(strike_out) as strike_out, sum(sacrifice_bunt) as sacrifice_bunt, sum(sacrifice_fly) as sacrifice_fly, round((sum(hits)+sum(walk)+sum(plunked))/(sum(at_stroke)+sum(walk)+sum(plunked)+sum(sacrifice_fly)), 3) as OBP, round((sum(hits)-sum(doubles)-sum(triples)-sum(homerun)+sum(doubles)*2+sum(triples)*3+sum(homerun)*4)/sum(at_stroke), 3) as SLG, round((sum(hits)+sum(walk)+sum(plunked))/(sum(at_stroke)+sum(walk)+sum(plunked)+sum(sacrifice_fly))+(sum(hits)-sum(doubles)-sum(triples)-sum(homerun)+sum(doubles)*2+sum(triples)*3+sum(homerun)*4)/sum(at_stroke), 3) as OPS"))
                            ->where("player_id", "=", "$player_id")
                            ->whereNull('delete_flag')
                            ->whereBetween("match_id", [2011000000, 2011999999])
                            ->get();
        $pitching_data_all = DB::table('t_match_pitchings')
                            ->select(DB::raw('count(match_id) as games'), DB::raw('sum(innings)+floor(sum(coalesce(fraction_innings,0))/3) as innings'), DB::raw('sum(coalesce(fraction_innings,0))%3 as fraction_innings'), DB::raw('sum(at_bat) as at_bat'), DB::raw('sum(hits) as hits'), DB::raw('sum(homerun) as homerun'), DB::raw('sum(strike_out) as strike_out'), DB::raw('sum(walk) as walk'), DB::raw('sum(plunked) as plunked'), DB::raw('sum(run) as run'), DB::raw('sum(earned_run) as earned_run'), DB::raw('round(sum(earned_run)/(sum(innings)*3+sum(coalesce(fraction_innings,0)))*21,2) as ERA'), DB::raw('sum(throw_full_innings) as throw_full_innings'), DB::raw('sum(win) as win'), DB::raw('sum(lose) as lose'), DB::raw('sum(hold) as hold'), DB::raw('sum(save) as save'))
                            ->where("player_id", "=", "$player_id")
                            ->whereNull('delete_flag')
                            ->get();
        $pitching_data_2021 = DB::table('t_match_pitchings')
                            ->select(DB::raw('count(match_id) as games'), DB::raw('sum(innings)+floor(sum(coalesce(fraction_innings,0))/3) as innings'), DB::raw('sum(coalesce(fraction_innings,0))%3 as fraction_innings'), DB::raw('sum(at_bat) as at_bat'), DB::raw('sum(hits) as hits'), DB::raw('sum(homerun) as homerun'), DB::raw('sum(strike_out) as strike_out'), DB::raw('sum(walk) as walk'), DB::raw('sum(plunked) as plunked'), DB::raw('sum(run) as run'), DB::raw('sum(earned_run) as earned_run'), DB::raw('round(sum(earned_run)/(sum(innings)*3+sum(coalesce(fraction_innings,0)))*21,2) as ERA'), DB::raw('sum(throw_full_innings) as throw_full_innings'), DB::raw('sum(win) as win'), DB::raw('sum(lose) as lose'), DB::raw('sum(hold) as hold'), DB::raw('sum(save) as save'))
                            ->where("player_id", "=", "$player_id")
                            ->whereNull('delete_flag')
                            ->whereBetween("match_id", [2021000000, 2021999999])
                            ->get();
        $pitching_data_2020 = DB::table('t_match_pitchings')
                            ->select(DB::raw('count(match_id) as games'), DB::raw('sum(innings)+floor(sum(coalesce(fraction_innings,0))/3) as innings'), DB::raw('sum(coalesce(fraction_innings,0))%3 as fraction_innings'), DB::raw('sum(at_bat) as at_bat'), DB::raw('sum(hits) as hits'), DB::raw('sum(homerun) as homerun'), DB::raw('sum(strike_out) as strike_out'), DB::raw('sum(walk) as walk'), DB::raw('sum(plunked) as plunked'), DB::raw('sum(run) as run'), DB::raw('sum(earned_run) as earned_run'), DB::raw('round(sum(earned_run)/(sum(innings)*3+sum(coalesce(fraction_innings,0)))*21,2) as ERA'), DB::raw('sum(throw_full_innings) as throw_full_innings'), DB::raw('sum(win) as win'), DB::raw('sum(lose) as lose'), DB::raw('sum(hold) as hold'), DB::raw('sum(save) as save'))
                            ->where("player_id", "=", "$player_id")
                            ->whereNull('delete_flag')
                            ->whereBetween("match_id", [2020000000, 2020999999])
                            ->get();
        $pitching_data_2019 = DB::table('t_match_pitchings')
                            ->select(DB::raw('count(match_id) as games'), DB::raw('sum(innings)+floor(sum(coalesce(fraction_innings,0))/3) as innings'), DB::raw('sum(coalesce(fraction_innings,0))%3 as fraction_innings'), DB::raw('sum(at_bat) as at_bat'), DB::raw('sum(hits) as hits'), DB::raw('sum(homerun) as homerun'), DB::raw('sum(strike_out) as strike_out'), DB::raw('sum(walk) as walk'), DB::raw('sum(plunked) as plunked'), DB::raw('sum(run) as run'), DB::raw('sum(earned_run) as earned_run'), DB::raw('round(sum(earned_run)/(sum(innings)*3+sum(coalesce(fraction_innings,0)))*21,2) as ERA'), DB::raw('sum(throw_full_innings) as throw_full_innings'), DB::raw('sum(win) as win'), DB::raw('sum(lose) as lose'), DB::raw('sum(hold) as hold'), DB::raw('sum(save) as save'))
                            ->where("player_id", "=", "$player_id")
                            ->whereNull('delete_flag')
                            ->whereBetween("match_id", [2019000000, 2019999999])
                            ->get();
        $pitching_data_2018 = DB::table('t_match_pitchings')
                            ->select(DB::raw('count(match_id) as games'), DB::raw('sum(innings)+floor(sum(coalesce(fraction_innings,0))/3) as innings'), DB::raw('sum(coalesce(fraction_innings,0))%3 as fraction_innings'), DB::raw('sum(at_bat) as at_bat'), DB::raw('sum(hits) as hits'), DB::raw('sum(homerun) as homerun'), DB::raw('sum(strike_out) as strike_out'), DB::raw('sum(walk) as walk'), DB::raw('sum(plunked) as plunked'), DB::raw('sum(run) as run'), DB::raw('sum(earned_run) as earned_run'), DB::raw('round(sum(earned_run)/(sum(innings)*3+sum(coalesce(fraction_innings,0)))*21,2) as ERA'), DB::raw('sum(throw_full_innings) as throw_full_innings'), DB::raw('sum(win) as win'), DB::raw('sum(lose) as lose'), DB::raw('sum(hold) as hold'), DB::raw('sum(save) as save'))
                            ->where("player_id", "=", "$player_id")
                            ->whereNull('delete_flag')
                            ->whereBetween("match_id", [2018000000, 2018999999])
                            ->get();
        $pitching_data_2017 = DB::table('t_match_pitchings')
                            ->select(DB::raw('count(match_id) as games'), DB::raw('sum(innings)+floor(sum(coalesce(fraction_innings,0))/3) as innings'), DB::raw('sum(coalesce(fraction_innings,0))%3 as fraction_innings'), DB::raw('sum(at_bat) as at_bat'), DB::raw('sum(hits) as hits'), DB::raw('sum(homerun) as homerun'), DB::raw('sum(strike_out) as strike_out'), DB::raw('sum(walk) as walk'), DB::raw('sum(plunked) as plunked'), DB::raw('sum(run) as run'), DB::raw('sum(earned_run) as earned_run'), DB::raw('round(sum(earned_run)/(sum(innings)*3+sum(coalesce(fraction_innings,0)))*21,2) as ERA'), DB::raw('sum(throw_full_innings) as throw_full_innings'), DB::raw('sum(win) as win'), DB::raw('sum(lose) as lose'), DB::raw('sum(hold) as hold'), DB::raw('sum(save) as save'))
                            ->where("player_id", "=", "$player_id")
                            ->whereNull('delete_flag')
                            ->whereBetween("match_id", [2017000000, 2017999999])
                            ->get();
        $pitching_data_2016 = DB::table('t_match_pitchings')
                            ->select(DB::raw('count(match_id) as games'), DB::raw('sum(innings)+floor(sum(coalesce(fraction_innings,0))/3) as innings'), DB::raw('sum(coalesce(fraction_innings,0))%3 as fraction_innings'), DB::raw('sum(at_bat) as at_bat'), DB::raw('sum(hits) as hits'), DB::raw('sum(homerun) as homerun'), DB::raw('sum(strike_out) as strike_out'), DB::raw('sum(walk) as walk'), DB::raw('sum(plunked) as plunked'), DB::raw('sum(run) as run'), DB::raw('sum(earned_run) as earned_run'), DB::raw('round(sum(earned_run)/(sum(innings)*3+sum(coalesce(fraction_innings,0)))*21,2) as ERA'), DB::raw('sum(throw_full_innings) as throw_full_innings'), DB::raw('sum(win) as win'), DB::raw('sum(lose) as lose'), DB::raw('sum(hold) as hold'), DB::raw('sum(save) as save'))
                            ->where("player_id", "=", "$player_id")
                            ->whereNull('delete_flag')
                            ->whereBetween("match_id", [2016000000, 2016999999])
                            ->get();
        $pitching_data_2015 = DB::table('t_match_pitchings')
                            ->select(DB::raw('count(match_id) as games'), DB::raw('sum(innings)+floor(sum(coalesce(fraction_innings,0))/3) as innings'), DB::raw('sum(coalesce(fraction_innings,0))%3 as fraction_innings'), DB::raw('sum(at_bat) as at_bat'), DB::raw('sum(hits) as hits'), DB::raw('sum(homerun) as homerun'), DB::raw('sum(strike_out) as strike_out'), DB::raw('sum(walk) as walk'), DB::raw('sum(plunked) as plunked'), DB::raw('sum(run) as run'), DB::raw('sum(earned_run) as earned_run'), DB::raw('round(sum(earned_run)/(sum(innings)*3+sum(coalesce(fraction_innings,0)))*21,2) as ERA'), DB::raw('sum(throw_full_innings) as throw_full_innings'), DB::raw('sum(win) as win'), DB::raw('sum(lose) as lose'), DB::raw('sum(hold) as hold'), DB::raw('sum(save) as save'))
                            ->where("player_id", "=", "$player_id")
                            ->whereNull('delete_flag')
                            ->whereBetween("match_id", [2015000000, 2015999999])
                            ->get();
        $pitching_data_2014 = DB::table('t_match_pitchings')
                            ->select(DB::raw('count(match_id) as games'), DB::raw('sum(innings)+floor(sum(coalesce(fraction_innings,0))/3) as innings'), DB::raw('sum(coalesce(fraction_innings,0))%3 as fraction_innings'), DB::raw('sum(at_bat) as at_bat'), DB::raw('sum(hits) as hits'), DB::raw('sum(homerun) as homerun'), DB::raw('sum(strike_out) as strike_out'), DB::raw('sum(walk) as walk'), DB::raw('sum(plunked) as plunked'), DB::raw('sum(run) as run'), DB::raw('sum(earned_run) as earned_run'), DB::raw('round(sum(earned_run)/(sum(innings)*3+sum(coalesce(fraction_innings,0)))*21,2) as ERA'), DB::raw('sum(throw_full_innings) as throw_full_innings'), DB::raw('sum(win) as win'), DB::raw('sum(lose) as lose'), DB::raw('sum(hold) as hold'), DB::raw('sum(save) as save'))
                            ->where("player_id", "=", "$player_id")
                            ->whereNull('delete_flag')
                            ->whereBetween("match_id", [2014000000, 2014999999])
                            ->get();
        $pitching_data_2013 = DB::table('t_match_pitchings')
                            ->select(DB::raw('count(match_id) as games'), DB::raw('sum(innings)+floor(sum(coalesce(fraction_innings,0))/3) as innings'), DB::raw('sum(coalesce(fraction_innings,0))%3 as fraction_innings'), DB::raw('sum(at_bat) as at_bat'), DB::raw('sum(hits) as hits'), DB::raw('sum(homerun) as homerun'), DB::raw('sum(strike_out) as strike_out'), DB::raw('sum(walk) as walk'), DB::raw('sum(plunked) as plunked'), DB::raw('sum(run) as run'), DB::raw('sum(earned_run) as earned_run'), DB::raw('round(sum(earned_run)/(sum(innings)*3+sum(coalesce(fraction_innings,0)))*21,2) as ERA'), DB::raw('sum(throw_full_innings) as throw_full_innings'), DB::raw('sum(win) as win'), DB::raw('sum(lose) as lose'), DB::raw('sum(hold) as hold'), DB::raw('sum(save) as save'))
                            ->where("player_id", "=", "$player_id")
                            ->whereNull('delete_flag')
                            ->whereBetween("match_id", [2013000000, 2013999999])
                            ->get();
        $pitching_data_2012 = DB::table('t_match_pitchings')
                            ->select(DB::raw('count(match_id) as games'), DB::raw('sum(innings)+floor(sum(coalesce(fraction_innings,0))/3) as innings'), DB::raw('sum(coalesce(fraction_innings,0))%3 as fraction_innings'), DB::raw('sum(at_bat) as at_bat'), DB::raw('sum(hits) as hits'), DB::raw('sum(homerun) as homerun'), DB::raw('sum(strike_out) as strike_out'), DB::raw('sum(walk) as walk'), DB::raw('sum(plunked) as plunked'), DB::raw('sum(run) as run'), DB::raw('sum(earned_run) as earned_run'), DB::raw('round(sum(earned_run)/(sum(innings)*3+sum(coalesce(fraction_innings,0)))*21,2) as ERA'), DB::raw('sum(throw_full_innings) as throw_full_innings'), DB::raw('sum(win) as win'), DB::raw('sum(lose) as lose'), DB::raw('sum(hold) as hold'), DB::raw('sum(save) as save'))
                            ->where("player_id", "=", "$player_id")
                            ->whereNull('delete_flag')
                            ->whereBetween("match_id", [2012000000, 2012999999])
                            ->get();
        $pitching_data_2011 = DB::table('t_match_pitchings')
                            ->select(DB::raw('count(match_id) as games'), DB::raw('sum(innings)+floor(sum(coalesce(fraction_innings,0))/3) as innings'), DB::raw('sum(coalesce(fraction_innings,0))%3 as fraction_innings'), DB::raw('sum(at_bat) as at_bat'), DB::raw('sum(hits) as hits'), DB::raw('sum(homerun) as homerun'), DB::raw('sum(strike_out) as strike_out'), DB::raw('sum(walk) as walk'), DB::raw('sum(plunked) as plunked'), DB::raw('sum(run) as run'), DB::raw('sum(earned_run) as earned_run'), DB::raw('round(sum(earned_run)/(sum(innings)*3+sum(coalesce(fraction_innings,0)))*21,2) as ERA'), DB::raw('sum(throw_full_innings) as throw_full_innings'), DB::raw('sum(win) as win'), DB::raw('sum(lose) as lose'), DB::raw('sum(hold) as hold'), DB::raw('sum(save) as save'))
                            ->where("player_id", "=", "$player_id")
                            ->whereNull('delete_flag')
                            ->whereBetween("match_id", [2011000000, 2011999999])
                            ->get();

        $scoring_data = DB::table('t_match_masters')
                        ->select('*')
                        ->where("scorer_id", "=", "$player_id")
                        ->whereNull('delete_flag')
                        ->get();

        $authoring_data = DB::table('t_match_reports')
                        ->where("author_id", "=", "$player_id")
                        ->whereNull('delete_flag')
                        ->get();

        //配列にまとめる
        $data = array (
            'member'=>$member,
            'batting_data_all'=>$batting_data_all,
            'batting_data_2021'=>$batting_data_2021,
            'batting_data_2020'=>$batting_data_2020,
            'batting_data_2019'=>$batting_data_2019,
            'batting_data_2018'=>$batting_data_2018,
            'batting_data_2017'=>$batting_data_2017,
            'batting_data_2016'=>$batting_data_2016,
            'batting_data_2015'=>$batting_data_2015,
            'batting_data_2014'=>$batting_data_2014,
            'batting_data_2013'=>$batting_data_2013,
            'batting_data_2012'=>$batting_data_2012,
            'batting_data_2011'=>$batting_data_2011,
            'pitching_data_2021'=>$pitching_data_2021,
            'pitching_data_2020'=>$pitching_data_2020,
            'pitching_data_2019'=>$pitching_data_2019,
            'pitching_data_2018'=>$pitching_data_2018,
            'pitching_data_2017'=>$pitching_data_2017,
            'pitching_data_2016'=>$pitching_data_2016,
            'pitching_data_2015'=>$pitching_data_2015,
            'pitching_data_2014'=>$pitching_data_2014,
            'pitching_data_2013'=>$pitching_data_2013,
            'pitching_data_2012'=>$pitching_data_2012,
            'pitching_data_2011'=>$pitching_data_2011,
        );
        return $data;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
