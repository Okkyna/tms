<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\M_player;
use App\T_match_master;
use App\T_match_batting;
use App\T_match_pitching;
use Carbon\Carbon;

class ResttitleholderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //タイトルホルダー
        //現在年を取得
        $year = Carbon::now()->year;

        //期間指定が可能な形式に変数を加工する。
        $from = str_pad($year, 10, 0, STR_PAD_RIGHT);
        $to   = str_pad($year, 10, 9, STR_PAD_RIGHT);

        //現在時点消化試合数
        $team_games = DB::table('t_match_masters')
                        ->select(DB::raw('count(match_id) as team_games'))
                        ->whereBetween('match_id',[$from,$to])
                        ->whereNotNull('result')
                        ->get();

        //規定打数・規定投球回数取得
        $rule1 = DB::table('t_match_masters')
                ->select(DB::RAW('count(match_id) as rule1'))
                ->whereBetween('match_id', [$from,$to])
                ->whereNotNull('result')
                ->get();

        $avg = DB::table('t_match_battings')
                ->leftjoin('m_players','t_match_battings.player_id','=','m_players.player_id')
                ->select('m_players.player_id', 'm_players.name', 'm_players.number', 'm_players.sort_number', 'm_players.player_category', DB::raw('count(match_id) as games'), DB::raw('sum(at_bat) as at_bat'), DB::raw('sum(at_stroke) as at_stroke'), DB::raw('sum(hits) as hits'), DB::raw('sum(doubles) as doubles'), DB::raw('sum(triples) as triples'), DB::raw('sum(homerun) as homerun'), DB::raw('round(sum(hits)/sum(at_stroke),3) as AVG'), DB::raw('sum(rbi) as rbi'), DB::raw('sum(run) as run'), DB::raw('sum(steal) as steal'), DB::raw('sum(walk) as walk'), DB::raw('sum(plunked) as plunked'), DB::raw('sum(strike_out) as strike_out'), DB::raw('sum(sacrifice_bunt) as sacrifice_bunt'), DB::raw('sum(sacrifice_fly) as sacrifice_fly'), DB::raw('round((sum(hits)+sum(walk)+sum(plunked)) / (sum(at_stroke)+sum(walk)+sum(plunked)+sum(sacrifice_fly)),3) as OBP'),DB::raw('round((sum(hits)-sum(doubles)-sum(triples)-sum(homerun)+sum(doubles)*2+sum(triples)*3+sum(homerun)*4)/sum(at_stroke),3) as SLG'), DB::raw('round((sum(hits)+sum(walk)+sum(plunked))/(sum(at_stroke)+sum(walk)+sum(plunked)+sum(sacrifice_fly))+(sum(hits)-sum(doubles)-sum(triples)-sum(homerun)+sum(doubles)*2+sum(triples)*3+sum(homerun)*4)/sum(at_stroke),3) as OPS'))
                ->whereBetween('match_id', [$from,$to])
                ->groupBy('m_players.player_id', 'm_players.name', 'm_players.number', 'm_players.sort_number', 'm_players.player_category')
                ->havingRaw('at_stroke > ?',[$rule1[0]->rule1])
                ->orderByRaw('AVG desc')
                ->limit(3)
                ->get();

        $homerun = DB::table('t_match_battings')
                    ->leftjoin('m_players','t_match_battings.player_id','=','m_players.player_id')
                    ->select('m_players.player_id', 'm_players.name', 'm_players.number', 'm_players.sort_number', 'm_players.player_category', DB::raw('count(match_id) as games'), DB::raw('sum(at_bat) as at_bat'), DB::raw('sum(at_stroke) as at_stroke'), DB::raw('sum(hits) as hits'), DB::raw('sum(doubles) as doubles'), DB::raw('sum(triples) as triples'), DB::raw('sum(homerun) as homerun'), DB::raw('round(sum(hits)/sum(at_stroke),3) as AVG'), DB::raw('sum(rbi) as rbi'), DB::raw('sum(run) as run'), DB::raw('sum(steal) as steal'), DB::raw('sum(walk) as walk'), DB::raw('sum(plunked) as plunked'), DB::raw('sum(strike_out) as strike_out'), DB::raw('sum(sacrifice_bunt) as sacrifice_bunt'), DB::raw('sum(sacrifice_fly) as sacrifice_fly'), DB::raw('round((sum(hits)+sum(walk)+sum(plunked)) / (sum(at_stroke)+sum(walk)+sum(plunked)+sum(sacrifice_fly)),3) as OBP'),DB::raw('round((sum(hits)-sum(doubles)-sum(triples)-sum(homerun)+sum(doubles)*2+sum(triples)*3+sum(homerun)*4)/sum(at_stroke),3) as SLG'), DB::raw('round((sum(hits)+sum(walk)+sum(plunked))/(sum(at_stroke)+sum(walk)+sum(plunked)+sum(sacrifice_fly))+(sum(hits)-sum(doubles)-sum(triples)-sum(homerun)+sum(doubles)*2+sum(triples)*3+sum(homerun)*4)/sum(at_stroke),3) as OPS'))
                    ->whereBetween('match_id', [$from,$to])
                    ->groupBy('m_players.player_id', 'm_players.name', 'm_players.number', 'm_players.sort_number', 'm_players.player_category')
                    ->orderByRaw('homerun desc')
                    ->limit(3)
                    ->get();

        $rbi = DB::table('t_match_battings')
                ->leftjoin('m_players','t_match_battings.player_id','=','m_players.player_id')
                ->select('m_players.player_id', 'm_players.name', 'm_players.number', 'm_players.sort_number', 'm_players.player_category', DB::raw('count(match_id) as games'), DB::raw('sum(at_bat) as at_bat'), DB::raw('sum(at_stroke) as at_stroke'), DB::raw('sum(hits) as hits'), DB::raw('sum(doubles) as doubles'), DB::raw('sum(triples) as triples'), DB::raw('sum(homerun) as homerun'), DB::raw('round(sum(hits)/sum(at_stroke),3) as AVG'), DB::raw('sum(rbi) as rbi'), DB::raw('sum(run) as run'), DB::raw('sum(steal) as steal'), DB::raw('sum(walk) as walk'), DB::raw('sum(plunked) as plunked'), DB::raw('sum(strike_out) as strike_out'), DB::raw('sum(sacrifice_bunt) as sacrifice_bunt'), DB::raw('sum(sacrifice_fly) as sacrifice_fly'), DB::raw('round((sum(hits)+sum(walk)+sum(plunked)) / (sum(at_stroke)+sum(walk)+sum(plunked)+sum(sacrifice_fly)),3) as OBP'),DB::raw('round((sum(hits)-sum(doubles)-sum(triples)-sum(homerun)+sum(doubles)*2+sum(triples)*3+sum(homerun)*4)/sum(at_stroke),3) as SLG'), DB::raw('round((sum(hits)+sum(walk)+sum(plunked))/(sum(at_stroke)+sum(walk)+sum(plunked)+sum(sacrifice_fly))+(sum(hits)-sum(doubles)-sum(triples)-sum(homerun)+sum(doubles)*2+sum(triples)*3+sum(homerun)*4)/sum(at_stroke),3) as OPS'))
                ->whereBetween('match_id', [$from,$to])
                ->groupBy('m_players.player_id', 'm_players.name', 'm_players.number', 'm_players.sort_number', 'm_players.player_category')
                ->orderByRaw('rbi desc')
                ->limit(3)
                ->get();

        $steal = DB::table('t_match_battings')
                ->leftjoin('m_players','t_match_battings.player_id','=','m_players.player_id')
                ->select('m_players.player_id', 'm_players.name', 'm_players.number', 'm_players.sort_number', 'm_players.player_category', DB::raw('count(match_id) as games'), DB::raw('sum(at_bat) as at_bat'), DB::raw('sum(at_stroke) as at_stroke'), DB::raw('sum(hits) as hits'), DB::raw('sum(doubles) as doubles'), DB::raw('sum(triples) as triples'), DB::raw('sum(homerun) as homerun'), DB::raw('round(sum(hits)/sum(at_stroke),3) as AVG'), DB::raw('sum(rbi) as rbi'), DB::raw('sum(run) as run'), DB::raw('sum(steal) as steal'), DB::raw('sum(walk) as walk'), DB::raw('sum(plunked) as plunked'), DB::raw('sum(strike_out) as strike_out'), DB::raw('sum(sacrifice_bunt) as sacrifice_bunt'), DB::raw('sum(sacrifice_fly) as sacrifice_fly'), DB::raw('round((sum(hits)+sum(walk)+sum(plunked)) / (sum(at_stroke)+sum(walk)+sum(plunked)+sum(sacrifice_fly)),3) as OBP'),DB::raw('round((sum(hits)-sum(doubles)-sum(triples)-sum(homerun)+sum(doubles)*2+sum(triples)*3+sum(homerun)*4)/sum(at_stroke),3) as SLG'), DB::raw('round((sum(hits)+sum(walk)+sum(plunked))/(sum(at_stroke)+sum(walk)+sum(plunked)+sum(sacrifice_fly))+(sum(hits)-sum(doubles)-sum(triples)-sum(homerun)+sum(doubles)*2+sum(triples)*3+sum(homerun)*4)/sum(at_stroke),3) as OPS'))
                ->whereBetween('match_id', [$from,$to])
                ->groupBy('m_players.player_id', 'm_players.name', 'm_players.number', 'm_players.sort_number', 'm_players.player_category')
                ->orderByRaw('steal desc')
                ->limit(3)
                ->get();

        $era = DB::table('t_match_pitchings')
                ->leftjoin('m_players','t_match_pitchings.player_id','=','m_players.player_id')
                ->select('m_players.player_id', 'm_players.name', 'm_players.number', 'm_players.sort_number', 'm_players.player_category', DB::raw('count(match_id) as games'), DB::raw('sum(innings)+floor(sum(coalesce(fraction_innings,0))/3) as innings'), DB::raw('sum(coalesce(fraction_innings,0))%3 as fraction_innings'), DB::raw('sum(run) as run'), DB::raw('sum(earned_run) as earned_run'), DB::raw('round(sum(earned_run)/(sum(innings)*3+sum(coalesce(fraction_innings,0)))*21,2) as ERA'), DB::raw('sum(win) as win'), DB::raw('sum(lose) as lose'), DB::raw('sum(hold) as hold'), DB::raw('sum(save) as save'))
                ->whereBetween('match_id', [$from,$to])
                ->groupBy('m_players.player_id', 'm_players.name', 'm_players.number', 'm_players.sort_number', 'm_players.player_category')
                ->havingRaw('innings > ?',[$rule1[0]->rule1])
                ->orderByRaw('ERA asc')
                ->limit(3)
                ->get();

        $win = DB::table('t_match_pitchings')
                ->leftjoin('m_players','t_match_pitchings.player_id','=','m_players.player_id')
                ->select('m_players.player_id', 'm_players.name', 'm_players.number', 'm_players.sort_number', 'm_players.player_category', DB::raw('count(match_id) as games'), DB::raw('sum(innings)+floor(sum(coalesce(fraction_innings,0))/3) as innings'), DB::raw('sum(coalesce(fraction_innings,0))%3 as fraction_innings'), DB::raw('sum(run) as run'), DB::raw('sum(earned_run) as earned_run'), DB::raw('round(sum(earned_run)/(sum(innings)*3+sum(coalesce(fraction_innings,0)))*21,2) as ERA'), DB::raw('sum(win) as win'), DB::raw('sum(lose) as lose'), DB::raw('sum(hold) as hold'), DB::raw('sum(save) as save'))
                ->whereBetween('match_id', [$from,$to])
                ->groupBy('m_players.player_id', 'm_players.name', 'm_players.number', 'm_players.sort_number', 'm_players.player_category')
                ->orderByRaw('win desc')
                ->limit(3)
                ->get();

        $save = DB::table('t_match_pitchings')
                ->leftjoin('m_players','t_match_pitchings.player_id','=','m_players.player_id')
                ->select('m_players.player_id', 'm_players.name', 'm_players.number', 'm_players.sort_number', 'm_players.player_category', DB::raw('count(match_id) as games'), DB::raw('sum(innings)+floor(sum(coalesce(fraction_innings,0))/3) as innings'), DB::raw('sum(coalesce(fraction_innings,0))%3 as fraction_innings'), DB::raw('sum(run) as run'), DB::raw('sum(earned_run) as earned_run'), DB::raw('round(sum(earned_run)/(sum(innings)*3+sum(coalesce(fraction_innings,0)))*21,2) as ERA'), DB::raw('sum(win) as win'), DB::raw('sum(lose) as lose'), DB::raw('sum(hold) as hold'), DB::raw('sum(save) as save'))
                ->whereBetween('match_id', [$from,$to])
                ->groupBy('m_players.player_id', 'm_players.name', 'm_players.number', 'm_players.sort_number', 'm_players.player_category')
                ->orderByRaw('save desc')
                ->limit(3)
                ->get();
        //配列にまとめる
        $data = array (
            'avg'=>$avg,
            'homerun'=>$homerun,
            'rbi'=>$rbi,
            'steal'=>$steal,
            'era'=>$era,
            'win'=>$win,
            'save'=>$save
        );
        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($year)
    {
         //期間指定が可能な形式に変数を加工する。
        $from = str_pad($year, 10, 0, STR_PAD_RIGHT);
        $to   = str_pad($year, 10, 9, STR_PAD_RIGHT);

        //現在時点消化試合数
        $team_games = DB::table('t_match_masters')
                        ->select(DB::raw('count(match_id) as team_games'))
                        ->whereBetween('match_id',[$from,$to])
                        ->whereNotNull('result')
                        ->get();

        //規定打数・規定投球回数取得
        $rule1 = DB::table('t_match_masters')
                ->select(DB::RAW('count(match_id) as rule1'))
                ->whereBetween('match_id', [$from,$to])
                ->whereNotNull('result')
                ->get();

        $avg = DB::table('t_match_battings')
                ->leftjoin('m_players','t_match_battings.player_id','=','m_players.player_id')
                ->select('m_players.player_id', 'm_players.name', 'm_players.number', 'm_players.sort_number', 'm_players.player_category', DB::raw('count(match_id) as games'), DB::raw('sum(at_bat) as at_bat'), DB::raw('sum(at_stroke) as at_stroke'), DB::raw('sum(hits) as hits'), DB::raw('sum(doubles) as doubles'), DB::raw('sum(triples) as triples'), DB::raw('sum(homerun) as homerun'), DB::raw('round(sum(hits)/sum(at_stroke),3) as AVG'), DB::raw('sum(rbi) as rbi'), DB::raw('sum(run) as run'), DB::raw('sum(steal) as steal'), DB::raw('sum(walk) as walk'), DB::raw('sum(plunked) as plunked'), DB::raw('sum(strike_out) as strike_out'), DB::raw('sum(sacrifice_bunt) as sacrifice_bunt'), DB::raw('sum(sacrifice_fly) as sacrifice_fly'), DB::raw('round((sum(hits)+sum(walk)+sum(plunked)) / (sum(at_stroke)+sum(walk)+sum(plunked)+sum(sacrifice_fly)),3) as OBP'),DB::raw('round((sum(hits)-sum(doubles)-sum(triples)-sum(homerun)+sum(doubles)*2+sum(triples)*3+sum(homerun)*4)/sum(at_stroke),3) as SLG'), DB::raw('round((sum(hits)+sum(walk)+sum(plunked))/(sum(at_stroke)+sum(walk)+sum(plunked)+sum(sacrifice_fly))+(sum(hits)-sum(doubles)-sum(triples)-sum(homerun)+sum(doubles)*2+sum(triples)*3+sum(homerun)*4)/sum(at_stroke),3) as OPS'))
                ->whereBetween('match_id', [$from,$to])
                ->groupBy('m_players.player_id', 'm_players.name', 'm_players.number', 'm_players.sort_number', 'm_players.player_category')
                ->havingRaw('at_stroke > ?',[$rule1[0]->rule1])
                ->orderByRaw('AVG desc')
                ->limit(3)
                ->get();

        $homerun = DB::table('t_match_battings')
                    ->leftjoin('m_players','t_match_battings.player_id','=','m_players.player_id')
                    ->select('m_players.player_id', 'm_players.name', 'm_players.number', 'm_players.sort_number', 'm_players.player_category', DB::raw('count(match_id) as games'), DB::raw('sum(at_bat) as at_bat'), DB::raw('sum(at_stroke) as at_stroke'), DB::raw('sum(hits) as hits'), DB::raw('sum(doubles) as doubles'), DB::raw('sum(triples) as triples'), DB::raw('sum(homerun) as homerun'), DB::raw('round(sum(hits)/sum(at_stroke),3) as AVG'), DB::raw('sum(rbi) as rbi'), DB::raw('sum(run) as run'), DB::raw('sum(steal) as steal'), DB::raw('sum(walk) as walk'), DB::raw('sum(plunked) as plunked'), DB::raw('sum(strike_out) as strike_out'), DB::raw('sum(sacrifice_bunt) as sacrifice_bunt'), DB::raw('sum(sacrifice_fly) as sacrifice_fly'), DB::raw('round((sum(hits)+sum(walk)+sum(plunked)) / (sum(at_stroke)+sum(walk)+sum(plunked)+sum(sacrifice_fly)),3) as OBP'),DB::raw('round((sum(hits)-sum(doubles)-sum(triples)-sum(homerun)+sum(doubles)*2+sum(triples)*3+sum(homerun)*4)/sum(at_stroke),3) as SLG'), DB::raw('round((sum(hits)+sum(walk)+sum(plunked))/(sum(at_stroke)+sum(walk)+sum(plunked)+sum(sacrifice_fly))+(sum(hits)-sum(doubles)-sum(triples)-sum(homerun)+sum(doubles)*2+sum(triples)*3+sum(homerun)*4)/sum(at_stroke),3) as OPS'))
                    ->whereBetween('match_id', [$from,$to])
                    ->groupBy('m_players.player_id', 'm_players.name', 'm_players.number', 'm_players.sort_number', 'm_players.player_category')
                    ->orderByRaw('homerun desc')
                    ->limit(3)
                    ->get();

        $rbi = DB::table('t_match_battings')
                ->leftjoin('m_players','t_match_battings.player_id','=','m_players.player_id')
                ->select('m_players.player_id', 'm_players.name', 'm_players.number', 'm_players.sort_number', 'm_players.player_category', DB::raw('count(match_id) as games'), DB::raw('sum(at_bat) as at_bat'), DB::raw('sum(at_stroke) as at_stroke'), DB::raw('sum(hits) as hits'), DB::raw('sum(doubles) as doubles'), DB::raw('sum(triples) as triples'), DB::raw('sum(homerun) as homerun'), DB::raw('round(sum(hits)/sum(at_stroke),3) as AVG'), DB::raw('sum(rbi) as rbi'), DB::raw('sum(run) as run'), DB::raw('sum(steal) as steal'), DB::raw('sum(walk) as walk'), DB::raw('sum(plunked) as plunked'), DB::raw('sum(strike_out) as strike_out'), DB::raw('sum(sacrifice_bunt) as sacrifice_bunt'), DB::raw('sum(sacrifice_fly) as sacrifice_fly'), DB::raw('round((sum(hits)+sum(walk)+sum(plunked)) / (sum(at_stroke)+sum(walk)+sum(plunked)+sum(sacrifice_fly)),3) as OBP'),DB::raw('round((sum(hits)-sum(doubles)-sum(triples)-sum(homerun)+sum(doubles)*2+sum(triples)*3+sum(homerun)*4)/sum(at_stroke),3) as SLG'), DB::raw('round((sum(hits)+sum(walk)+sum(plunked))/(sum(at_stroke)+sum(walk)+sum(plunked)+sum(sacrifice_fly))+(sum(hits)-sum(doubles)-sum(triples)-sum(homerun)+sum(doubles)*2+sum(triples)*3+sum(homerun)*4)/sum(at_stroke),3) as OPS'))
                ->whereBetween('match_id', [$from,$to])
                ->groupBy('m_players.player_id', 'm_players.name', 'm_players.number', 'm_players.sort_number', 'm_players.player_category')
                ->orderByRaw('rbi desc')
                ->limit(3)
                ->get();

        $steal = DB::table('t_match_battings')
                ->leftjoin('m_players','t_match_battings.player_id','=','m_players.player_id')
                ->select('m_players.player_id', 'm_players.name', 'm_players.number', 'm_players.sort_number', 'm_players.player_category', DB::raw('count(match_id) as games'), DB::raw('sum(at_bat) as at_bat'), DB::raw('sum(at_stroke) as at_stroke'), DB::raw('sum(hits) as hits'), DB::raw('sum(doubles) as doubles'), DB::raw('sum(triples) as triples'), DB::raw('sum(homerun) as homerun'), DB::raw('round(sum(hits)/sum(at_stroke),3) as AVG'), DB::raw('sum(rbi) as rbi'), DB::raw('sum(run) as run'), DB::raw('sum(steal) as steal'), DB::raw('sum(walk) as walk'), DB::raw('sum(plunked) as plunked'), DB::raw('sum(strike_out) as strike_out'), DB::raw('sum(sacrifice_bunt) as sacrifice_bunt'), DB::raw('sum(sacrifice_fly) as sacrifice_fly'), DB::raw('round((sum(hits)+sum(walk)+sum(plunked)) / (sum(at_stroke)+sum(walk)+sum(plunked)+sum(sacrifice_fly)),3) as OBP'),DB::raw('round((sum(hits)-sum(doubles)-sum(triples)-sum(homerun)+sum(doubles)*2+sum(triples)*3+sum(homerun)*4)/sum(at_stroke),3) as SLG'), DB::raw('round((sum(hits)+sum(walk)+sum(plunked))/(sum(at_stroke)+sum(walk)+sum(plunked)+sum(sacrifice_fly))+(sum(hits)-sum(doubles)-sum(triples)-sum(homerun)+sum(doubles)*2+sum(triples)*3+sum(homerun)*4)/sum(at_stroke),3) as OPS'))
                ->whereBetween('match_id', [$from,$to])
                ->groupBy('m_players.player_id', 'm_players.name', 'm_players.number', 'm_players.sort_number', 'm_players.player_category')
                ->orderByRaw('steal desc')
                ->limit(3)
                ->get();

        $era = DB::table('t_match_pitchings')
                ->leftjoin('m_players','t_match_pitchings.player_id','=','m_players.player_id')
                ->select('m_players.player_id', 'm_players.name', 'm_players.number', 'm_players.sort_number', 'm_players.player_category', DB::raw('count(match_id) as games'), DB::raw('sum(innings)+floor(sum(coalesce(fraction_innings,0))/3) as innings'), DB::raw('sum(coalesce(fraction_innings,0))%3 as fraction_innings'), DB::raw('sum(run) as run'), DB::raw('sum(earned_run) as earned_run'), DB::raw('round(sum(earned_run)/(sum(innings)*3+sum(coalesce(fraction_innings,0)))*21,2) as ERA'), DB::raw('sum(win) as win'), DB::raw('sum(lose) as lose'), DB::raw('sum(hold) as hold'), DB::raw('sum(save) as save'))
                ->whereBetween('match_id', [$from,$to])
                ->groupBy('m_players.player_id', 'm_players.name', 'm_players.number', 'm_players.sort_number', 'm_players.player_category')
                ->havingRaw('innings > ?',[$rule1[0]->rule1])
                ->orderByRaw('ERA asc')
                ->limit(3)
                ->get();

        $win = DB::table('t_match_pitchings')
                ->leftjoin('m_players','t_match_pitchings.player_id','=','m_players.player_id')
                ->select('m_players.player_id', 'm_players.name', 'm_players.number', 'm_players.sort_number', 'm_players.player_category', DB::raw('count(match_id) as games'), DB::raw('sum(innings)+floor(sum(coalesce(fraction_innings,0))/3) as innings'), DB::raw('sum(coalesce(fraction_innings,0))%3 as fraction_innings'), DB::raw('sum(run) as run'), DB::raw('sum(earned_run) as earned_run'), DB::raw('round(sum(earned_run)/(sum(innings)*3+sum(coalesce(fraction_innings,0)))*21,2) as ERA'), DB::raw('sum(win) as win'), DB::raw('sum(lose) as lose'), DB::raw('sum(hold) as hold'), DB::raw('sum(save) as save'))
                ->whereBetween('match_id', [$from,$to])
                ->groupBy('m_players.player_id', 'm_players.name', 'm_players.number', 'm_players.sort_number', 'm_players.player_category')
                ->orderByRaw('win desc')
                ->limit(3)
                ->get();

        $save = DB::table('t_match_pitchings')
                ->leftjoin('m_players','t_match_pitchings.player_id','=','m_players.player_id')
                ->select('m_players.player_id', 'm_players.name', 'm_players.number', 'm_players.sort_number', 'm_players.player_category', DB::raw('count(match_id) as games'), DB::raw('sum(innings)+floor(sum(coalesce(fraction_innings,0))/3) as innings'), DB::raw('sum(coalesce(fraction_innings,0))%3 as fraction_innings'), DB::raw('sum(run) as run'), DB::raw('sum(earned_run) as earned_run'), DB::raw('round(sum(earned_run)/(sum(innings)*3+sum(coalesce(fraction_innings,0)))*21,2) as ERA'), DB::raw('sum(win) as win'), DB::raw('sum(lose) as lose'), DB::raw('sum(hold) as hold'), DB::raw('sum(save) as save'))
                ->whereBetween('match_id', [$from,$to])
                ->groupBy('m_players.player_id', 'm_players.name', 'm_players.number', 'm_players.sort_number', 'm_players.player_category')
                ->orderByRaw('save desc')
                ->limit(3)
                ->get();
        //配列にまとめる
        $data = array (
            'avg'=>$avg,
            'homerun'=>$homerun,
            'rbi'=>$rbi,
            'steal'=>$steal,
            'era'=>$era,
            'win'=>$win,
            'save'=>$save
        );
        return $data;
}

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
