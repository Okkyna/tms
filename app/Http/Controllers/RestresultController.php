<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;

class RestresultController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //１．初期表示…現在年を取得する。
        $year_from = Carbon::now()->year;
        $year_to   = Carbon::now()->year;
        $title = $year_from.'年'; //タイトル表示用変数

        //期間指定が可能な形式に変数を加工する。
        $from = str_pad($year_from, 10, 0, STR_PAD_RIGHT);
        $to   = str_pad($year_to, 10, 9, STR_PAD_RIGHT);

        $results = DB::table('t_match_masters')
                    ->leftjoin('m_grounds','t_match_masters.ground_id','=','m_grounds.ground_id')
                    ->select('*')
                    ->whereNull('t_match_masters.delete_flag')
                    ->whereNotNull('first_total')
                    ->whereNotNull('after_total')
                    ->whereDate('date','<','Carbon::now()')
                    ->whereBetween('match_id',[$from,$to])
                    ->orderBy('match_id', 'desc')
                    ->get();

        $win = DB::table('t_match_masters')
                ->select(DB::raw('count(result) as win'))
                ->where('result','1')
                ->whereBetween('match_id',[$from,$to])
                ->get();

        $draw = DB::table('t_match_masters')
                ->select(DB::raw('count(result) as draw'))
                ->where('result','2')
                ->whereBetween('match_id',[$from,$to])
                ->get();

        $lose = DB::table('t_match_masters')
                ->select(DB::raw('count(result) as lose'))
                ->where('result','3')
                ->whereBetween('match_id',[$from,$to])
                ->get();
        return $results->toArray();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($year)
    {
        $temp = $year;
        $year_from = $temp;
        $year_to   = $temp;


        //期間指定が可能な形式に変数を加工する。
        $from = str_pad($year_from, 10, 0, STR_PAD_RIGHT);
        $to   = str_pad($year_to, 10, 9, STR_PAD_RIGHT);

        $results = DB::table('t_match_masters')
                    ->leftjoin('m_grounds','t_match_masters.ground_id','=','m_grounds.ground_id')
                    ->select('*')
                    ->whereNull('t_match_masters.delete_flag')
                    ->whereNotNull('first_total')
                    ->whereNotNull('after_total')
                    ->whereDate('date','<','Carbon::now()')
                    ->whereBetween('match_id',[$from,$to])
                    ->orderBy('match_id', 'desc')
                    ->get();

        $win = DB::table('t_match_masters')
                ->select(DB::raw('count(result) as win'))
                ->where('result','1')
                ->whereBetween('match_id',[$from,$to])
                ->get();

        $draw = DB::table('t_match_masters')
                ->select(DB::raw('count(result) as draw'))
                ->where('result','2')
                ->whereBetween('match_id',[$from,$to])
                ->get();

        $lose = DB::table('t_match_masters')
                ->select(DB::raw('count(result) as lose'))
                ->where('result','3')
                ->whereBetween('match_id',[$from,$to])
                ->get();

        return $results->toArray();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
