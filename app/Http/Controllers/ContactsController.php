<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ContactRequest;
use App\Http\Controllers\Controller;
use App\Contact;

use Illuminate\Support\Facades\Mail;

class ContactsController extends Controller
{
    public function index()
    {
        $types = Contact::$types;

        return view('contacts.index', compact('types'));
    }

    public function confirm(ContactRequest $request)
    {
        $contact = new Contact($request->all());

        //お問い合わせ種類を配列から文字列に
        $type = '';
        if (isset($request->type)) {
            $type = implode(', ',$request->type);
        }

        return view('contacts.confirm', compact('contact','type'));
    }

    public function complete(ContactRequest $request)
    {
        $input = $request->except('action');

        if ($request->action === '戻る') {
            return redirect()->action('ContactsController@index')->withInput($input);
        }

        //チェックボックスを,区切りの文字列に
        if (isset($request->type)) {
            $request->merge(['type' => implode(', ', $request->type)]);
        }

        //データを保存
        Contact::create($request->all());

        //二重送信防止
        $request->session()->regenerateToken();

        //送信メール
//        \Mail::send(new \App\Mail\Contact([
//            'to' => $request->email,
//            'to_name' => $request->name,
//            'from' => 'no-reply@twinbee.info',
//            'from_name' => '大阪の草野球チームTwinbee',
//            'subject' => 'お問い合わせありがとうございました。',
//            'type' => $request->type,
//            'body' => $request->body
//        ]));
//
        // 受信メール
        \Mail::send(new \App\Mail\Contact([
            'to' => 'oki.takumi@gmail.com',
            'to_name' => '大阪の草野球チームTwinbee',
            'from' => 'web@twinbee.info',
            'from_name' => $request->name,
            'subject' => 'サイトからのお問い合わせ',
            'type' => $request->type,
            'body' => $request->body
        ], 'from'));

        return view('contacts.complete');

    }
}
