<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\T_match_master;
use App\M_ground;
use Carbon\Carbon;


class ScheduleController extends Controller
{
    public function index() {

        $schedule = DB::table('t_match_masters')
                    ->leftjoin('m_grounds','t_match_masters.ground_id','=','m_grounds.ground_id')
                    ->select('*')
                    ->where('date','>=',Carbon::today())
                    ->orderBy('match_id','asc')
                    ->get();

        return view('schedule.index', compact('schedule'));

    }
}
