<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;

class RestresultdetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($match_id)
    {
        //スコア取得
        $score = DB::table('t_match_masters')
                    ->join('m_grounds','t_match_masters.ground_id','=','m_grounds.ground_id')
                    ->select('*')
                    ->where('t_match_masters.match_id',$match_id)
                    ->first();

        if ($match_id > 2018000000)
        {
            //スコアボード下成績表示用対象開始年月日取得
            $temp_from = substr($match_id, 0, 4);
            $from = str_pad($temp_from, 10, 0, STR_PAD_RIGHT);
            $to = $match_id;

            //スコアボード下投手成績集計
            if ($score->result == 1)
            {
                $responsible_pitcher = DB::table('t_match_pitchings')
                                        ->select('player_id')
                                        ->where('match_id',$match_id)
                                        ->where('win',1)
                                        ->first();
                $responsible_stats = DB::table('t_match_pitchings')
                            ->join('m_players','t_match_pitchings.player_id','=','m_players.player_id')
                            ->select('t_match_pitchings.player_id', 'name', DB::raw('sum(win) as win'), DB::raw('sum(lose) as lose'), DB::raw('sum(save) as save'))
                            ->whereBetween('match_id', [$from,$to])
                            ->where('t_match_pitchings.player_id',$responsible_pitcher->player_id)
                            ->groupBy('t_match_pitchings.player_id', 'name')
                            ->first();
                $relief_pitcher =DB::table('t_match_pitchings')
                                ->select('player_id')
                                ->where('match_id',$match_id)
                                ->where('save',1)
                                ->first();
                if (isset($relief_pitcher->player_id))
                {
                    $relief_stats = DB::table('t_match_pitchings')
                    ->join('m_players','t_match_pitchings.player_id','=','m_players.player_id')
                    ->select('t_match_pitchings.player_id', 'name', DB::raw('sum(win) as win'), DB::raw('sum(lose) as lose'), DB::raw('sum(save) as save'))
                    ->whereBetween('match_id', [$from,$to])
                    ->where('t_match_pitchings.player_id',$relief_pitcher->player_id)
                    ->groupBy('t_match_pitchings.player_id', 'name')
                    ->first();
                }
            }
            elseif ($score->result == 3)
            {
                $relief_pitcher = NULL;
                $relief_stats = NULL;
                $responsible_pitcher = DB::table('t_match_pitchings')
                                        ->select('player_id')
                                        ->where('match_id',$match_id)
                                        ->where('lose',1)
                                        ->first();
                $responsible_stats = DB::table('t_match_pitchings')
                            ->join('m_players','t_match_pitchings.player_id','=','m_players.player_id')
                            ->select('t_match_pitchings.player_id', 'name', DB::raw('sum(win) as win'), DB::raw('sum(lose) as lose'), DB::raw('sum(save) as save'))
                            ->whereBetween('match_id', [$from,$to])
                            ->where('t_match_pitchings.player_id',$responsible_pitcher->player_id)
                            ->groupBy('t_match_pitchings.player_id', 'name')
                            ->first();
            }
            //スコアボード下本塁打表示用
            $temp1 = DB::table('t_match_battings')
                        ->select ('player_id',DB::raw('sum(homerun) as total_homerun'))
                        ->whereBetween('match_id', [$from,$to])
                        ->groupBy('t_match_battings.player_id');
            $temp2 = DB::table('t_match_battings')
                        ->select('player_id','homerun')
                        ->where('match_id',$match_id);
            $homerun_stats = DB::table('m_players')
                                ->select('m_players.player_id','m_players.name','t1.total_homerun','t2.homerun')
                                ->leftJoin(DB::raw('('. $temp1->toSql() .') as t1'), 'm_players.player_id', '=', 't1.player_id')
                                ->mergeBindings($temp1)
                                ->leftJoin(DB::raw('('. $temp2->toSql() .') as t2'), 'm_players.player_id', '=', 't2.player_id')
                                ->mergeBindings($temp2)
                                ->where('t2.homerun','>','0')
                                ->get();
            //打撃成績取得
            $temp_batting_data = DB::table('t_match_battings');
            $batting_data = $temp_batting_data
                            ->join('m_players','t_match_battings.player_id','=','m_players.player_id')
                            ->select('batting_order','t_match_battings.player_id', 'name', 'position', DB::raw('sum(at_bat) as at_bat'), DB::raw('sum(at_stroke) as at_stroke'), DB::raw('sum(hits) as hits'), DB::raw('sum(doubles) as doubles'), DB::raw('sum(triples) as triples'), DB::raw('sum(homerun) as homerun'), DB::raw('sum(rbi) as rbi'), DB::raw('sum(run) as run'), DB::raw('sum(steal) as steal'), DB::raw('sum(walk) as walk'), DB::raw('sum(plunked) as plunked'), DB::raw('sum(strike_out) as strike_out'), DB::raw('sum(sacrifice_bunt) as sacrifice_bunt'), DB::raw('sum(sacrifice_fly) as sacrifice_fly'))
                            ->where('match_id', $match_id)
                            ->groupBy('batting_order', 'player_id','name','position')
                            ->orderBy('batting_order','asc')
                            ->get();
            //投球成績取得
            $temp_pitching_data = DB::table('t_match_pitchings');
            $pitching_data = $temp_pitching_data
                                ->join('m_players','t_match_pitchings.player_id','=','m_players.player_id')
                                ->select('pitching_order','t_match_pitchings.player_id', 'name', DB::raw('sum(innings) as innings'), DB::raw('sum(fraction_innings) as fraction_innings'), DB::raw('sum(hits) as hits'), DB::raw('sum(strike_out) as strike_out'), DB::raw('sum(walk) as walk'), DB::raw('sum(plunked) as plunked'), DB::raw('sum(run) as run'), DB::raw('sum(earned_run) as earned_run'), DB::raw('sum(win) as win'), DB::raw('sum(lose) as lose'), DB::raw('sum(hold) as hold'), DB::raw('sum(save) as save'))
                                ->where('match_id', $match_id)
                                ->groupBy('pitching_order', 'player_id','name')
                                ->orderBy('pitching_order','asc')
                                ->get();
            //戦評取得
            $report = DB::table('t_match_reports')
                        ->leftjoin('m_players','t_match_reports.author_id','=','m_players.player_id')
                        ->select('match_id','digest','locker_room','author_id','photo_main', 't_match_reports.photo_1 as photo_1','t_match_reports.photo_2 as photo_2','t_match_reports.photo_3 as photo_3','t_match_reports.photo_4 as photo_4','t_match_reports.photo_5 as photo_5','name','number')
                        ->where('match_id', $match_id)
                        ->first();
            //配列にまとめる
            $data = array (
                'score'=>$score,
                'responsible_pitcher'=>$responsible_pitcher,
                'responsible_stats'=>$responsible_stats,
                'relief_pitcher'=>$relief_pitcher,
                'relief_stats'=>$relief_stats,
                'homerun_stats'=>$homerun_stats,
                'batting_data'=>$batting_data,
                'pitching_data'=>$pitching_data,
                'report'=>$report
            );
            return $data;
        }

        else {
            //戦評取得
            $report = DB::table('t_match_reports')
            ->leftjoin('m_players','t_match_reports.author_id','=','m_players.player_id')
            ->select('match_id','digest','locker_room','author_id','photo_main', 't_match_reports.photo_1 as photo_1','t_match_reports.photo_2 as photo_2','t_match_reports.photo_3 as photo_3','t_match_reports.photo_4 as photo_4','t_match_reports.photo_5 as photo_5','name','number')
            ->where('match_id', $match_id)
            ->first();
            //配列にまとめる
            $data = array (
                'score'=>$score,
                'report'=>$report
            );
            return $data;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
