<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;

class ReststatsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //１．初期表示…現在年を取得する。
        $year_from = Carbon::now()->year;
        $year_to   = Carbon::now()->year;
        $title = $year_from.'年'; //タイトル表示用変数

        //期間指定が可能な形式に変数を加工する。
        $from = str_pad($year_from, 10, 0, STR_PAD_RIGHT);
        $to   = str_pad($year_to, 10, 9, STR_PAD_RIGHT);

        //打撃成績
        $batting_stats = DB::table('t_match_battings')
                            ->leftjoin('m_players','t_match_battings.player_id','=','m_players.player_id')
                            ->select('m_players.player_id', 'm_players.name', 'm_players.number', 'm_players.sort_number', 'm_players.player_category', DB::raw('count(match_id) as games'), DB::raw('sum(at_bat) as at_bat'), DB::raw('sum(at_stroke) as at_stroke'), DB::raw('sum(hits) as hits'), DB::raw('sum(doubles) as doubles'), DB::raw('sum(triples) as triples'), DB::raw('sum(homerun) as homerun'), DB::raw('round(sum(hits)/sum(at_stroke),3) as AVG'), DB::raw('sum(rbi) as rbi'), DB::raw('sum(run) as run'), DB::raw('sum(steal) as steal'), DB::raw('sum(walk) as walk'), DB::raw('sum(plunked) as plunked'), DB::raw('sum(strike_out) as strike_out'), DB::raw('sum(sacrifice_bunt) as sacrifice_bunt'), DB::raw('sum(sacrifice_fly) as sacrifice_fly'), DB::raw('round((sum(hits)+sum(walk)+sum(plunked)) / (sum(at_stroke)+sum(walk)+sum(plunked)+sum(sacrifice_fly)),3) as OBP'),DB::raw('round((sum(hits)-sum(doubles)-sum(triples)-sum(homerun)+sum(doubles)*2+sum(triples)*3+sum(homerun)*4)/sum(at_stroke),3) as SLG'), DB::raw('round((sum(hits)+sum(walk)+sum(plunked))/(sum(at_stroke)+sum(walk)+sum(plunked)+sum(sacrifice_fly))+(sum(hits)-sum(doubles)-sum(triples)-sum(homerun)+sum(doubles)*2+sum(triples)*3+sum(homerun)*4)/sum(at_stroke),3) as OPS'))
                            ->whereNull('m_players.delete_flag')
                            ->where('m_players.player_category',1)
                            ->whereBetween('match_id', [$from,$to])
                            ->groupBy('m_players.player_id', 'm_players.name', 'm_players.number', 'm_players.sort_number', 'm_players.player_category')
                            ->orderBy('m_players.sort_number','asc')
                            ->get();

        //投球成績
        $pitching_stats = DB::table('t_match_pitchings')
        ->leftjoin('m_players','t_match_pitchings.player_id','=','m_players.player_id')
        ->select('m_players.player_id', 'm_players.name', 'm_players.number', 'm_players.sort_number', 'm_players.player_category', DB::raw('count(match_id) as games'), DB::raw('sum(innings)+floor(sum(coalesce(fraction_innings,0))/3) as innings'), DB::raw('sum(coalesce(fraction_innings,0))%3 as fraction_innings'), DB::raw('sum(run) as run'), DB::raw('sum(earned_run) as earned_run'), DB::raw('round(sum(earned_run)/(sum(innings)*3+sum(coalesce(fraction_innings,0)))*21,2) as ERA'), DB::raw('sum(win) as win'), DB::raw('sum(lose) as lose'), DB::raw('sum(hold) as hold'), DB::raw('sum(save) as save'))
        ->whereNull('m_players.delete_flag')
        ->where('m_players.player_category',1)
        ->whereBetween('match_id', [$from,$to])
        ->groupBy('m_players.player_id', 'm_players.name', 'm_players.number', 'm_players.sort_number', 'm_players.player_category')
        ->orderBy('m_players.sort_number','asc')
        ->get();

        $data = array (
            'batting_stats'=>$batting_stats,
            'pitching_stats'=>$pitching_stats
        );
        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($year)
    {
        $tmp = $year;
        $year_from = $tmp;
        $year_to   = $tmp;


        //期間指定が可能な形式に変数を加工する。
        $from = str_pad($year_from, 10, 0, STR_PAD_RIGHT);
        $to   = str_pad($year_to, 10, 9, STR_PAD_RIGHT);

        //外部結合用変数
        $temp1 = DB::table('m_players')
                    ->select('*');
        //打撃成績
        $batting_stats = DB::table('t_match_battings')
                            ->leftjoin('m_players','t_match_battings.player_id','=','m_players.player_id')
                            ->select('m_players.player_id', 'm_players.name', 'm_players.number', 'm_players.sort_number', 'm_players.player_category', DB::raw('count(match_id) as games'), DB::raw('sum(at_bat) as at_bat'), DB::raw('sum(at_stroke) as at_stroke'), DB::raw('sum(hits) as hits'), DB::raw('sum(doubles) as doubles'), DB::raw('sum(triples) as triples'), DB::raw('sum(homerun) as homerun'), DB::raw('round(sum(hits)/sum(at_stroke),3) as AVG'), DB::raw('sum(rbi) as rbi'), DB::raw('sum(run) as run'), DB::raw('sum(steal) as steal'), DB::raw('sum(walk) as walk'), DB::raw('sum(plunked) as plunked'), DB::raw('sum(strike_out) as strike_out'), DB::raw('sum(sacrifice_bunt) as sacrifice_bunt'), DB::raw('sum(sacrifice_fly) as sacrifice_fly'), DB::raw('round((sum(hits)+sum(walk)+sum(plunked)) / (sum(at_stroke)+sum(walk)+sum(plunked)+sum(sacrifice_fly)),3) as OBP'),DB::raw('round((sum(hits)-sum(doubles)-sum(triples)-sum(homerun)+sum(doubles)*2+sum(triples)*3+sum(homerun)*4)/sum(at_stroke),3) as SLG'), DB::raw('round((sum(hits)+sum(walk)+sum(plunked))/(sum(at_stroke)+sum(walk)+sum(plunked)+sum(sacrifice_fly))+(sum(hits)-sum(doubles)-sum(triples)-sum(homerun)+sum(doubles)*2+sum(triples)*3+sum(homerun)*4)/sum(at_stroke),3) as OPS'))
                            ->whereNull('m_players.delete_flag')
                            ->where('m_players.player_category',1)
                            ->whereBetween('match_id', [$from,$to])
                            ->groupBy('m_players.player_id', 'm_players.name', 'm_players.number', 'm_players.sort_number', 'm_players.player_category')
                            ->orderBy('m_players.sort_number','asc')
                            ->get();

        //投球成績
        //注意！fraction_inningsの値がNULLの場合、計算がおかしくなる。
        $pitching_stats = DB::table('t_match_pitchings')
        ->leftjoin('m_players','t_match_pitchings.player_id','=','m_players.player_id')
        ->select('m_players.player_id', 'm_players.name', 'm_players.number', 'm_players.sort_number', 'm_players.player_category', DB::raw('count(match_id) as games'), DB::raw('sum(innings)+floor(sum(coalesce(fraction_innings,0))/3) as innings'), DB::raw('sum(coalesce(fraction_innings,0))%3 as fraction_innings'), DB::raw('sum(run) as run'), DB::raw('sum(earned_run) as earned_run'), DB::raw('round(sum(earned_run)/(sum(innings)*3+sum(coalesce(fraction_innings,0)))*21,2) as ERA'), DB::raw('sum(win) as win'), DB::raw('sum(lose) as lose'), DB::raw('sum(hold) as hold'), DB::raw('sum(save) as save'))
        ->whereNull('m_players.delete_flag')
        ->where('m_players.player_category',1)
        ->whereBetween('match_id', [$from,$to])
        ->groupBy('m_players.player_id', 'm_players.name', 'm_players.number', 'm_players.sort_number', 'm_players.player_category')
        ->orderBy('m_players.sort_number','asc')
        ->get();

        $data = array (
            'batting_stats'=>$batting_stats,
            'pitching_stats'=>$pitching_stats
        );
        return $data;

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
