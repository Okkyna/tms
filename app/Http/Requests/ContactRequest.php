<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => 'required',
            'type.*' => 'in:入団に関する問い合わせ,対戦に関する問い合わせ,その他',
            'name' => 'required|max:50',
            'email' => 'required|email',
            'body' => 'required|max:1000'
        ];
    }

    public function attributes()
    {
        return [
            'type' => 'お問い合わせ種別',
            'name' => 'お名前',
            'email' => 'メールアドレス',
            'body' => '内容'
        ];
    }
}
