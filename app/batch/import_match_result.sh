#!/bin/sh

#0.変数設定
app_path=/Applications/MAMP/htdocs/tms-laravel/tms-laravel
work_path=/Applications/MAMP/htdocs/tms-laravel/tms-laravel/storage/temp
backup_path=/Applications/MAMP/htdocs/tms-laravel/tms-laravel/storage/backup
file_date=$1
set -eu

# 1.取り込み事前準備
## 1-1.DBダンプ
cd $app_path
mysqldump -u batch_user tms_laravel > $backup_path/tms_laravel_$file_date.dump

## 1-2.文字コード変換
csv_type_match=$(nkf --guess $work_path/t_match_sjis$file_date.csv)
csv_type_batting=$(nkf --guess $work_path/t_batting_sjis$file_date.csv)
csv_type_pitching=$(nkf --guess $work_path/t_pitching_sjis$file_date.csv)

## 1-3.文字コードチェック
if [ "$csv_type_match" = "Shift_JIS (CR)" ] || [ "$csv_type_match" = "ASCII (CR)" ] || [ "$csv_type_match" = "Shift_JIS (CRLF)" ] || [ "$csv_type_match" = "ASCII (CRLF)" ]; then
  nkf -wd $work_path/t_match_sjis$file_date.csv > $work_path/t_match_$file_date.csv
  else
    echo "【エラー】t_match_sjis$file_date.csvの文字コードが"$csv_type_match"です。"
    exit
fi
if [ "$csv_type_batting" = "Shift_JIS (CR)" ] || [ "$csv_type_batting" = "ASCII (CR)" ] || [ "$csv_type_batting" = "Shift_JIS (CRLF)" ] || [ "$csv_type_batting" = "ASCII (CRLF)" ]; then
  nkf -wd $work_path/t_batting_sjis$file_date.csv > $work_path/t_batting_$file_date.csv
  else
    echo "【エラー】t_batting_sjis$file_date.csvの文字コードが"$csv_type_batting"です。"
    exit
fi
if [ "$csv_type_pitching" = "Shift_JIS (CR)" ] || [ "$csv_type_pitching" = "ASCII (CR)" ] || [ "$csv_type_pitching" = "Shift_JIS (CRLF)" ] || [ "$csv_type_pitching" = "ASCII (CRLF)" ]; then
  nkf -wd $work_path/t_pitching_sjis$file_date.csv > $work_path/t_pitching_$file_date.csv
  else
    echo "【エラー】t_pitching_sjis$file_date.csvの文字コードが"$csv_type_pitching"です。"
    exit
fi

# 2. データ取り込み処理
## 2-1. MySQL接続
CMD_MYSQL="mysql --local_infile=1 -u batch_user tms_laravel"

## 2-2. t_match_mastersインポート
$CMD_MYSQL <<EOF
TRUNCATE TABLE w_match_masters;
EOF

$CMD_MYSQL <<EOF
load data local infile "$work_path/t_match_$file_date.csv"
 into table w_match_masters
 fields terminated by ','
 ignore 1 lines
 (match_id, match_category, w_match_masters.date, start_time, end_time, ground_id, opponent, result, match_order, @var_front_1, @var_back_1, @var_front_2, @var_back_2, @var_front_3, @var_back_3, @var_front_4, @var_back_4, @var_front_5, @var_back_5, @var_front_6, @var_back_6, @var_front_7, @var_back_7, @var_front_8, @var_back_8, @var_front_9, @var_back_9, first_total, after_total )
 set
 front_1 = case @var_front_1 when '' then NULL else @var_front_1 end,
 back_1= case @var_back_1 when '' then NULL else @var_back_1 end,
 front_2 = case @var_front_2 when '' then NULL else @var_front_2 end,
 back_2= case @var_back_2 when '' then NULL else @var_back_2 end,
 front_3 = case @var_front_3 when '' then NULL else @var_front_3 end,
 back_3= case @var_back_3 when '' then NULL else @var_back_3 end,
 front_4 = case @var_front_4 when '' then NULL else @var_front_4 end,
 back_4= case @var_back_4 when '' then NULL else @var_back_4 end,
 front_5 = case @var_front_5 when '' then NULL else @var_front_5 end,
 back_5= case @var_back_5 when '' then NULL else @var_back_5 end,
 front_6 = case @var_front_6 when '' then NULL else @var_front_6 end,
 back_6= case @var_back_6 when '' then NULL else @var_back_6 end,
 front_7 = case @var_front_7 when '' then NULL else @var_front_7 end,
 back_7= case @var_back_7 when '' then NULL else @var_back_7 end,
 front_8 = case @var_front_8 when '' then NULL else @var_front_8 end,
 back_8= case @var_back_8 when '' then NULL else @var_back_8 end,
 front_9 = case @var_front_9 when '' then NULL else @var_front_9 end,
 back_9= case @var_back_9 when '' then NULL else @var_back_9 end;
EOF

$CMD_MYSQL <<EOF
insert into t_match_masters select * from w_match_masters
on duplicate key update
 match_category = values(match_category),
 date = values(date),
 start_time = values(start_time),
 end_time = values(end_time),
 ground_id = values(ground_id),
 opponent = values(opponent),
 result = values(result),
 match_order = values(match_order),
 front_1 = values(front_1),
 back_1 = values(back_1),
 front_2 = values(front_2),
 back_2 = values(back_2),
 front_3 = values(front_3),
 back_3 = values(back_3),
 front_4 = values(front_4),
 back_4 = values(back_4),
 front_5 = values(front_5),
 back_5 = values(back_5),
 front_6 = values(front_6),
 back_6 = values(back_6),
 front_7 = values(front_7),
 back_7 = values(back_7),
 front_8 = values(front_8),
 back_8 = values(back_8),
 front_9 = values(front_9),
 back_9 = values(back_9),
 first_total = values(first_total),
 after_total = values(after_total);
EOF

## 2-3. t_match_battingsインポート
$CMD_MYSQL <<EOF
load data local infile "$work_path/t_batting_$file_date.csv"
 into table t_match_battings
 fields terminated by ','
 ignore 1 lines
 (match_id, batting_order, player_id, position, at_bat, at_stroke, hits, doubles, triples, homerun, rbi, run, steal, walk, plunked, strike_out, sacrifice_bunt, sacrifice_fly );
EOF

## 2-4. t_match_pitchingsインポート
$CMD_MYSQL <<EOF
load data local infile "$work_path/t_pitching_$file_date.csv"
 into table t_match_pitchings
 fields terminated by ','
 ignore 1 lines
 (match_id, pitching_order, player_id, innings, fraction_innings, t_match_pitchings.number, at_bat, hits, homerun, strike_out, walk, plunked, run, earned_run, throw_full_innings, win, lose, hold, save );
EOF

