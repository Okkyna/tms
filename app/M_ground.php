<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class M_ground extends Model
{
    protected $primaryKey = 'ground_id';

    public function t_match_masters()
    {
        return $this->hasOne('App\T_match_master');
    }
}
