<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Http\Request;

Route::get('/', 'IndexController@index');
Route::get('members', 'MembersController@index');
Route::get('members/{player_id}', 'MembersController@show');
Route::get('results', 'ResultsController@index');
Route::post('results', 'ResultsController@search');
Route::get('results/{match_id}', 'ResultsController@show');
Route::get('stats', 'StatsController@index');
Route::post('stats', 'StatsController@search');
Route::get('schedule', 'ScheduleController@index');

Route::get('contact', 'ContactsController@index');
Route::get('contact/confirm', 'ContactsController@confirm');
Route::post('contact/confirm', 'ContactsController@confirm');
Route::post('contact/complete', 'ContactsController@complete');

//静的ページ
Route::get('aboutus/',function() {
    return view('pages.aboutus'); //チーム紹介
});

Route::get('for_join/',function() {
    return view('pages.for_join'); //入団を希望される方へ
});

Route::get('match_make/',function() {
    return view('pages.match_make'); //対戦を希望されるチーム様へ
});



//Bulma検証用
Route::get('/styleguide', function() {
    return view('bulma');
    }
);

//API開発用
Route::resource('api/v1/player', 'RestplayerController');
Route::resource('api/v1/schedule', 'RestscheduleController');
Route::resource('api/v1/result', 'RestresultController');
Route::resource('api/v1/result_detail', 'RestresultdetailController');
Route::resource('api/v1/stats', 'ReststatsController');
Route::resource('api/v1/titleholder', 'ResttitleholderController');
