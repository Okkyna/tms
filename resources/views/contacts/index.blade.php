@extends('layout.base')

@section('title')
お問い合わせ｜大阪の草野球チームTwinbee
@endsection

@section('content')

<!-- 下層タイトル　-->
<div class="container is-fluid has-background-info pt-40 pb-40">
    <div class="container">
        <h1 class="title is-2 has-text-white">お問い合わせ</h1>
        <p class="subtitle is-6 has-text-white">こちらからツインビーに各種お問い合わせいただけます。</p>
    </div>
</div>

<div class="container">
    <section class="section">
        <p>入力項目は全て必須項目です。入力されましたら確認を押下してください。</p>

        {!! Form::open(['url' => 'contact/confirm', 'class' => 'mt-50']) !!}
            <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }} mb-20">
                {!! Form::label('type', 'お問い合わせ種別:', ['class' => 'control-label has-text-weight-bold']) !!}
                <div>
                    @foreach($types as $key => $value)
                        <label>
                            {!! Form::checkbox('type[]', $value) !!}
                            {{ $value }}
                        </label><br>
                    @endforeach
                    @if ($errors->has('type'))
                        <p class="has-text-danger">{{ $errors->first('type') }}</p>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} mb-20">
                {!! Form::label('name', 'お名前:', ['class' => 'control-label has-text-weight-bold']) !!}
                <div>
                    {!! Form::text('name', null, ['class' => 'form-control']) !!}
                    @if ($errors->has('name'))
                        <p class="has-text-danger">{{ $errors->first('name') }}</p>
                    @endif
                </div>
            </div>
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} mb-20">
                {!! Form::label('email', 'メールアドレス:', ['class' => 'control-label has-text-weight-bold']) !!}
                <div>
                    {!! Form::email('email', null, ['class' => 'form-control']) !!}
                    @if ($errors->has('email'))
                        <p class="has-text-danger">{{ $errors->first('email') }}</p>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('body') ? ' has-error' : '' }} mb-20">
                {!! Form::label('body', '内容:', ['class' => 'control-label has-text-weight-bold']) !!}
                <div class="col-sm-10">
                    {!! Form::textarea('body', null, ['class' => 'form-control']) !!}
                    @if ($errors->has('body'))
                        <p class="has-text-danger">{{ $errors->first('body') }}</p>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <div>
                    {!! Form::submit('確認', ['class' => 'button is-success']) !!}
                </div>
            </div>
            {!! Form::close() !!}
    </section>
</div>

@endsection
