@extends('layout.base')

@section('title')
お問い合わせ｜大阪の草野球チームTwinbee
@endsection

@section('content')

<!-- 下層タイトル　-->
<div class="container is-fluid has-background-info pt-40 pb-40">
    <div class="container">
        <section class="section">
            <h1 class="title is-2 has-text-white">お問い合わせ</h1>
            <p class="subtitle is-6 has-text-white">こちらからツインビーに各種お問い合わせいただけます。</p>
        </section>
    </div>
</div>

<div class="container">

<p>問い合わせの送信が完了しました。<br>
代表者より折り返しご連絡させていただきます。<br>
数日経っても連絡が無い場合、お手数ですが再度お問い合わせいただくようお願いいたします。
</p>

</div>

@endsection
