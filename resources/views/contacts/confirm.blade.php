@extends('layout.base')

@section('title')
お問い合わせ｜大阪の草野球チームTwinbee
@endsection

@section('content')

<!-- 下層タイトル　-->
<div class="container is-fluid has-background-info pt-40 pb-40">
    <div class="container">
        <h1 class="title is-2 has-text-white">お問い合わせ</h1>
        <p class="subtitle is-6 has-text-white">こちらからツインビーに各種お問い合わせいただけます。</p>
    </div>
</div>

<div class="container">
    <section class="section">
        <p>入力内容を確認いただき、問題なければ「送信」を押下してください。</p>
        <table class="table is-responsive is-bordered is-fullwidth mt-50">
            <tr>
                <th class="has-background-info has-text-white">お問い合わせ種別</th>
                <td>{{ $type }}</td>
            </tr>
            <tr>
                <th class="has-background-info has-text-white">お名前</th>
                <td>{{ $contact->name }}</td>
            </tr>
            <tr>
                <th class="has-background-info has-text-white">メールアドレス</th>
                <td>{{ $contact->email }}</td>
            </tr>
            <tr>
                <th class="has-background-info has-text-white">内容</th>
                <td>{{ $contact->body }}</td>
            </tr>
        </table>

        {!! Form::open(['url' => 'contact/complete','class' => 'form-horizontal','id' => 'post-input']) !!}

        @foreach($contact->getAttributes() as $key => $value)
            @if(isset($value))
                @if(is_array($value))
                    @foreach($value as $subValue)
                        <input name="{{ $key }}[]" type="hidden" value="{{ $subValue }}">
                    @endforeach
                @else
                    {!! Form::hidden($key, $value) !!}
                @endif

            @endif
        @endforeach

        {!! Form::submit('戻る', ['name' => 'action', 'class' => 'button is-light']) !!}
        {!! Form::submit('送信', ['name' => 'action', 'class' => 'button is-success']) !!}
        {!! Form::close() !!}

    </section>
</div>

@endsection
