@extends('layout.base')

@section('title')
{{ Carbon\Carbon::parse($score->date)->format('Y/m/d') }}&nbsp;{{ $score->opponent }}戦｜大阪の草野球チーム Twinbee（ツインビー）
@endsection

@section('content')

<!-- 下層タイトル　-->
<div class="container is-fluid has-background-info pt-40 pb-40">
    <div class="container">
        <h1 class="title is-2 has-text-white">試合結果</h1>
        <p class="subtitle is-6 has-text-white">ツインビーの試合結果をご覧いただけます。</p>
    </div>
</div>
<div class="container">
    <p class="has-background-grey has-text-centered has-text-white pt-20 pb-20 mt-50">本ページは旧システムからの移行の影響で表示崩れ・リンク切れ等がありますがご容赦ください。</p>
    <!-- スコアボード -->
    <div class="score-board-wrapper mb-20">
        <div class="score_board has-background-dark has-text-white">
            <p class="is-size-6">
                {{ Carbon\Carbon::parse($score->date)->format('Y/m/d') }}&nbsp;&nbsp;{{ Carbon\Carbon::parse($score->start_time)->format('H:i') }}〜{{ Carbon\Carbon::parse($score->end_time)->format('H:i') }}&nbsp;&nbsp;{{ $score->name }}&nbsp;&nbsp;
                @if($score->match_category == 201)
                    スカイカップ
                @elseif($score->match_category == 100)
                    練習試合
                @endif
            </p>
            <ul>
                <li class="team has-text-right is-size-5">
                    @if($score->match_order == 1)ツインビー
                    @elseif($score->match_order == 2){{ $score->opponent }}
                    @endif
                </li>
                <li class="score has-text-centered is-size-2 has-text-weight-bold">{{ $score->first_total }}</li>
                <li class="separator has-text-centered is-size-6">-</li>
                <li class="score has-text-centered is-size-2 has-text-weight-bold">{{ $score->after_total }}</li>
                <li class="team has-text-left is-size-5">
                    @if($score->match_order == 2)ツインビー
                    @elseif($score->match_order == 1){{ $score->opponent }}
                    @endif
                </li>
            </ul>
        </div>
    </div>

<!-- タブコンテンツ -->
<div id="tab-content">
    @isset($report->digest)
    <!-- ダイジェスト -->
    <div class="is-active" data-content="1">
        <div class="container">
            <section class="section pt-10">
                {!! $report->digest !!}
                @isset($report->author_id)
                    <p class="has-text-right">文責：#{{ $report->number }}&nbsp;{{ $report->name }}</p>
                @endisset
            </section>
        </div>
    </div>
    @endisset
 </div>

@endsection
