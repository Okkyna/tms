@extends('layout.base')

@section('title')
試合結果｜大阪の草野球チーム Twinbee
@endsection

@section('content')
<!-- 下層タイトル　-->
<div class="container is-fluid has-background-info pt-40 pb-40">
    <div class="container">
        <h1 class="title is-2 has-text-white">試合結果</h1>
        <p class="subtitle is-6 has-text-white">ツインビーの試合結果をご覧いただけます。</p>
    </div>
</div>

<!-- 検索エリア　-->
<div class="container">
    <section class="section">
        <div class="columns">
            <div class="column is-3 has-background-grey-light">
                <div class="search_title_wrapper">
                    <h2 class="title is-4 search_title has-text-white">絞り込み検索</h2>
                </div>
            </div>
            <div class="column has-background-white-ter">
                <form action="{{ url('/results') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="field">
                        <label class="label">年度選択</label>
                    </div>
                    <div class="control">
                        <div class="select is-rounded">
                            <select name="year_search" size="1">
                                <option disabled selected>選択してください。</option>
                                <option value="all">全て</option>
                                <option value="2020">2020年</option>
                                <option value="2019">2019年</option>
                                <option value="2018">2018年</option>
                                <option value="2017">2017年</option>
                                <option value="2016">2016年</option>
                                <option value="2015">2015年</option>
                                <option value="2014">2014年</option>
                                <option value="2013">2013年</option>
                                <option value="2012">2012年</option>
                                <option value="2011">2011年</option>
                                <option value="2010">2010年</option>
                            </select>
                        </div>
                    </div>
                    <div class="field mt-20">
                        <div class="control">
                            <button class="button is-success has-text-weight-bold" type="submit" name="search">検索</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>

<!-- 集計表示エリア -->
<div class="container">
    <section class="section">
        <h2 class="title is-3">{{ $title }}結果一覧</h2>
        <p class="is-size-4 has-text-weight-bold">{{ $win[0]->win }}勝{{ $lose[0]->lose }}敗{{ $draw[0]->draw }}引き分け</p>
    </section>
</div>

<!-- 検索結果表示エリア -->
<div class="container">
    <div class="columns is-multiline">
        @foreach($results as $r)
        <div class="column is-half">
            <a href="/results/{{ $r->match_id }}">
                <div class="score_panel_wrapper">
                    <div class="score_panel has-background-dark has-text-white">
                        <p class="is-size-6 has-text-centered">
                            {{ Carbon\Carbon::parse($r->date)->format('Y/m/d') }}&nbsp;&nbsp;{{ Carbon\Carbon::parse($r->start_time)->format('H:i') }}〜{{ Carbon\Carbon::parse($r->end_time)->format('H:i') }}&nbsp;&nbsp;
                            @if($r->match_category == 201)
                                スカイカップ
                            @elseif($r->match_category == 100)
                                練習試合
                            @endif
                        </p>
                        <ul>
                            <li class="team has-text-right is-size-5">
                                @if($r->match_order == 1)ツインビー
                                @elseif($r->match_order == 2){{ $r->opponent }}
                                @endif
                            </li>
                            <li class="score has-text-centered is-size-2 has-text-weight-bold">{{ $r->first_total }}</li>
                            <li class="separator has-text-centered is-size-6">-</li>
                            <li class="score has-text-centered is-size-2 has-text-weight-bold">{{ $r->after_total }}</li>
                            <li class="team has-text-left is-size-5">
                                @if($r->match_order == 2)ツインビー
                                @elseif($r->match_order == 1){{ $r->opponent }}
                                @endif
                            </li>
                        </ul>
                    </div>
                    <div class="score_link has-background-dark has-text-white has-text-centered">
                        <span class="icon"><i class="fas fa-angle-right fa-2x"></i></span>
                    </div>
                </div>
            </a>
        </div>
        @endforeach
    </div>
</div>
@endsection
