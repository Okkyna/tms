@extends('layout.base')

@section('title')
{{ Carbon\Carbon::parse($score->date)->format('Y/m/d') }}&nbsp;{{ $score->opponent }}戦｜大阪の草野球チーム Twinbee（ツインビー）
@endsection

@section('content')

<!-- 下層タイトル　-->
<div class="container is-fluid has-background-info pt-40 pb-40">
    <div class="container">
        <h1 class="title is-2 has-text-white">試合結果</h1>
        <p class="subtitle is-6 has-text-white">ツインビーの試合結果をご覧いただけます。</p>
    </div>
</div>
<div class="container">
        <!-- スコアボード -->
        <div class="score-board-wrapper mb-20 mt-50">
            <div class="score_board has-background-dark has-text-white">
                <p class="is-size-6">
                    {{ Carbon\Carbon::parse($score->date)->format('Y/m/d') }}&nbsp;&nbsp;{{ Carbon\Carbon::parse($score->start_time)->format('H:i') }}〜{{ Carbon\Carbon::parse($score->end_time)->format('H:i') }}&nbsp;&nbsp;{{ $score->name }}&nbsp;&nbsp;
                    @if($score->match_category == 201)
                        スカイカップ
                    @elseif($score->match_category == 100)
                        練習試合
                    @endif
                </p>
                <ul>
                    <li class="team has-text-right is-size-5">
                        @if($score->match_order == 1)ツインビー
                        @elseif($score->match_order == 2){{ $score->opponent }}
                        @endif
                    </li>
                    <li class="score has-text-centered is-size-2 has-text-weight-bold">{{ $score->first_total }}</li>
                    <li class="separator has-text-centered is-size-6">-</li>
                    <li class="score has-text-centered is-size-2 has-text-weight-bold">{{ $score->after_total }}</li>
                    <li class="team has-text-left is-size-5">
                        @if($score->match_order == 2)ツインビー
                        @elseif($score->match_order == 1){{ $score->opponent }}
                        @endif
                    </li>
                </ul>
            </div>
             <table class="table is-responsive is-bordered is-fullwidth">
                <thead>
                    <tr class="has-background-dark">
                        <th class="has-text-centered">チーム</th>
                        <th class="has-text-centered">1</th>
                        <th class="has-text-centered">2</th>
                        <th class="has-text-centered">3</th>
                        <th class="has-text-centered">4</th>
                        <th class="has-text-centered">5</th>
                        <th class="has-text-centered">6</th>
                        <th class="has-text-centered">7</th>
                        @isset( $score->front_8 )<th class="has-text-centered">8</th>@endisset
                        @isset( $score->front_9 )<th class="has-text-centered">9</th>@endisset
                        <th class="has-text-centered">R</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            @if($score->match_order == 1)ツインビー
                            @elseif($score->match_order == 2){{ $score->opponent }}
                            @endif
                        </td>
                        <td class="has-text-centered">{{ $score->front_1 }}</td>
                        <td class="has-text-centered">{{ $score->front_2 }}</td>
                        <td class="has-text-centered">{{ $score->front_3 }}</td>
                        <td class="has-text-centered">{{ $score->front_4 }}</td>
                        <td class="has-text-centered">{{ $score->front_5 }}</td>
                        <td class="has-text-centered">{{ $score->front_6 }}</td>
                        <td class="has-text-centered">{{ $score->front_7 }}</td>
                        @isset( $score->front_8 )<td class="has-text-centered">{{ $score->front_8 }}</td>@endisset
                        @isset( $score->front_9 )<td class="has-text-centered">{{ $score->front_9 }}</td>@endisset
                        <td class="has-text-centered">{{ $score->first_total }}</td>
                    </tr>
                    <tr>
                        <td>
                            @if($score->match_order == 1)
                                {{ $score->opponent }}
                            @elseif($score->match_order == 2)
                                ツインビー
                            @endif
                        <td class="has-text-centered">{{ $score->back_1 }}</td>
                        <td class="has-text-centered">{{ $score->back_2 }}</td>
                        <td class="has-text-centered">{{ $score->back_3 }}</td>
                        <td class="has-text-centered">{{ $score->back_4 }}</td>
                        <td class="has-text-centered">{{ $score->back_5 }}</td>
                        <td class="has-text-centered">{{ $score->back_6 }}</td>
                        <td class="has-text-centered">{{ $score->back_7 }}</td>
                        @isset( $score->back_8 )<td class="has-text-centered">{{ $score->back_8 }}</td>@endisset
                        @isset( $score->back_9 )<td class="has-text-centered">{{ $score->back_9 }}</td>@endisset
                        <td class="has-text-centered">{{ $score->after_total }}</td>
                    </tr>
                </tbody>
            </table>
        </div>

        <!-- 責任投手／本塁打　-->
        <div class="match_information">
            @if($score->result == 1)
                <p>勝利投手：{{ $responsible_stats->name }}&nbsp;{{ $responsible_stats->win }}勝{{ $responsible_stats->lose }}敗{{ $responsible_stats->save }}S</p>
                @isset($relief_stats)
                    <p>セーブ：{{ $relief_stats->name }}&nbsp;{{ $relief_stats->win }}勝{{ $relief_stats->lose }}敗{{ $relief_stats->save }}S </p>
                @endisset
            @elseif($score->result == 2)
            @elseif($score->result == 3)
                <p>敗戦投手：{{ $responsible_stats->name }}&nbsp;{{ $responsible_stats->win }}勝{{ $responsible_stats->lose }}敗{{ $responsible_stats->save }}S</p>
            @endif
            @isset($homerun_stats)
                @foreach($homerun_stats as $b)
                    @if ($loop->first)<p>本塁打：@endif
                        {{ $b->name }}{{ $b->total_homerun }}号&nbsp;
                    @if ($loop->last)</p>@endif
                @endforeach
            @endisset
        </div>
</div>

<!-- タブナビゲーション -->
<div class="container mt-50 mb-20">
    <div class="tabs is-centered is-boxed" id="tabs">
        <ul>
            @isset($report->digest)
                <li class="is-active" data-tab="1"><a>ダイジェスト</a></li>
                <li data-tab="2"><a>打撃成績</a></li>
            @endisset
            @empty($report->digest)
                <li class="is-active" data-tab="2"><a>打撃成績</a></li>
            @endempty
            <li data-tab="3"><a>投球成績</a></li>
            @isset($report->locker_room)
                <li data-tab="4"><a>ロッカールーム</a></li>
            @endisset
            @isset($report->photo_1)
                <li data-tab="5"><a>ギャラリー</a></li>
            @endisset
        </ul>
    </div>
</div>

<!-- タブコンテンツ -->
<div id="tab-content">
    @isset($report->digest)
    <!-- ダイジェスト -->
    <div class="is-active" data-content="1">
        <div class="container">
            <section class="section pt-10">
                {!! $report->digest !!}
                @isset($report->author_id)
                    <p class="has-text-right">文責：#{{ $report->number }}&nbsp;{{ $report->name }}</p>
                @endisset
            </section>
        </div>
    </div>
    <!-- 打撃成績　-->
    <div data-content="2">
    @endisset
    @empty($report->digest)
    <div class="is-active" data-content="2">
    @endempty
        <div class="container pt-10">
            <div class="of-scroll">
                <table class="table is-responsive is-bordered is-striped is-fullwidth">
                    <thead>
                        <tr class="has-background-info">
                            <th class="has-text-centered">打順</th>
                            <th class="has-text-centered">選手</th>
                            <th class="has-text-centered">守備</th>
                            <th class="has-text-centered">打席</th>
                            <th class="has-text-centered">打数</th>
                            <th class="has-text-centered">安打</th>
                            <th class="has-text-centered">二塁打</th>
                            <th class="has-text-centered">三塁打</th>
                            <th class="has-text-centered">本塁打</th>
                            <th class="has-text-centered">打点</th>
                            <th class="has-text-centered">得点</th>
                            <th class="has-text-centered">盗塁</th>
                            <th class="has-text-centered">四球</th>
                            <th class="has-text-centered">死球</th>
                            <th class="has-text-centered">三振</th>
                            <th class="has-text-centered">犠打</th>
                            <th class="has-text-centered">犠飛</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($batting_data as $b)
                        <tr>
                            <td class="has-text-centered">{{ $loop->iteration }}</td>
                            <td><a href="/members/{{ $b->player_id }}">{{ $b->name }}</a></td>
                            <td class="has-text-centered">{{ $b->position }}</td>
                            <td class="has-text-centered">{{ $b->at_bat }}</td>
                            <td class="has-text-centered">{{ $b->at_stroke }}</td>
                            <td class="has-text-centered">{{ $b->hits }}</td>
                            <td class="has-text-centered">{{ $b->doubles }}</td>
                            <td class="has-text-centered">{{ $b->triples }}</td>
                            <td class="has-text-centered">{{ $b->homerun }}</td>
                            <td class="has-text-centered">{{ $b->rbi }}</td>
                            <td class="has-text-centered">{{ $b->run }}</td>
                            <td class="has-text-centered">{{ $b->steal }}</td>
                            <td class="has-text-centered">{{ $b->walk }}</td>
                            <td class="has-text-centered">{{ $b->plunked }}</td>
                            <td class="has-text-centered">{{ $b->strike_out }}</td>
                            <td class="has-text-centered">{{ $b->sacrifice_bunt }}</td>
                            <td class="has-text-centered">{{ $b->sacrifice_fly }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- 投球成績　-->
    <div data-content="3">
        <div class="container pt-10">
            <div class="of-scroll">
                <table class="table is-responsive is-bordered is-striped is-fullwidth">
                    <thead>
                        <tr class="has-background-info">
                            <th class="has-text-centered">選手</th>
                            <th class="has-text-centered">投球回</th>
                            <th class="has-text-centered">被安打</th>
                            <th class="has-text-centered">奪三振</th>
                            <th class="has-text-centered">与四球</th>
                            <th class="has-text-centered">与死球</th>
                            <th class="has-text-centered">失点</th>
                            <th class="has-text-centered">自責点</th>
                            <th class="has-text-centered">責任</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($pitching_data as $p)
                        <tr>
                            <td>
                                <a href="/members/{{ $p->player_id }}">{{ $p->name }}</a>
                            </td>
                            <td class="has-text-centered">
                                {{ $p->innings }}
                                @if($p->fraction_innings > 0)
                                    &nbsp;{{ $p->fraction_innings }}/3
                                @endif
                            </td>
                            <td class="has-text-centered">{{ $p->hits }}</td>
                            <td class="has-text-centered">{{ $p->strike_out }}</td>
                            <td class="has-text-centered">{{ $p->walk }}</td>
                            <td class="has-text-centered">{{ $p->plunked }}</td>
                            <td class="has-text-centered">{{ $p->run }}</td>
                            <td class="has-text-centered">{{ $p->earned_run }}</td>
                            <td class="has-text-centered">
                                @if($p->win > 0)勝
                                @elseif($p->lose > 0)負
                                @elseif($p->hold > 0)H
                                @elseif($p->save > 0)S
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- ロッカールーム -->
    @isset($report->locker_room)
    <div data-content="4">
        <div class="container">
            <section class="section pt-10">
                {!! $report->locker_room !!}
            </section>
        </div>
    </div>
    @endisset
    <!-- ギャラリー -->
    @isset($report->photo_1)
    <div data-content="5">
        <div class="container">
            <section class="section pt=10">
                <div class="columns is-multiline">
                    <p>{{ $report->photo_1 }}</p>
                </div>
            </section>
        </div>
    </div>
    @endisset
 </div>

@endsection
