@extends('layout.base')

@section('title')
大阪の草野球チーム Twinbee（ツインビー）
@endsection

@section('content')


<!-- メインビジュアル -->
{{--
<div class="container is-fluid mb-50">
        <img src="/img/top/14811647.jpg">
</div> --}}

<section class="hero is-large is-dark is-bold mb-50">
        <div class="container">
            <img src="/img/top/main.png">
        </div>
</section>
<!-- //メインビジュアル -->

<!-- 最近の試合結果 -->
<div class="container mb-50">
    <h2 class="title is-3 has-text-centered ">最新の試合結果</h2>
    <div class="columns">
        @foreach($recently_results as $r)
        <div class="column">
            <a href="/results/{{ $r->match_id }}">
                <div class="score_panel_wrapper">
                    <div class="score_panel has-background-dark has-text-white">
                        <p class="is-size-6 has-text-centered">
                            {{ Carbon\Carbon::parse($r->date)->format('Y/m/d') }}&nbsp;&nbsp;{{ Carbon\Carbon::parse($r->start_time)->format('H:i') }}〜{{ Carbon\Carbon::parse($r->end_time)->format('H:i') }}&nbsp;&nbsp;
                            @if($r->match_category == 201)
                                スカイカップ
                            @elseif($r->match_category == 100)
                                練習試合
                            @endif
                        </p>
                        <ul>
                            <li class="team has-text-right is-size-5">
                                @if($r->match_order == 1)ツインビー
                                @elseif($r->match_order == 2){{ $r->opponent }}
                                @endif
                            </li>
                            <li class="score has-text-centered is-size-2 has-text-weight-bold">{{ $r->first_total }}</li>
                            <li class="separator has-text-centered is-size-6">-</li>
                            <li class="score has-text-centered is-size-2 has-text-weight-bold">{{ $r->after_total }}</li>
                            <li class="team has-text-left is-size-5">
                                @if($r->match_order == 2)ツインビー
                                @elseif($r->match_order == 1){{ $r->opponent }}
                                @endif
                            </li>
                        </ul>
                    </div>
                    <div class="score_link has-background-dark has-text-white has-text-centered">
                        <span class="icon"><i class="fas fa-angle-right fa-2x"></i></span>
                    </div>
                </div>
            </a>
        </div>
        @endforeach
    </div>
</div>
<!-- //最近の試合結果 -->

<!-- 活動予定 -->
<div class="container mb-50">
    <h2 class="title is-3 has-text-centered">直近の活動予定</h2>
    <div class="columns is-multiline">
        @foreach($schedule as $s)
        <div class="column is-half">
            <div class="schedule_panel has-background-dark has-text-white">
                @if($s->name)
                <div class="date has-text-centered ">{{ Carbon\Carbon::parse($s->date)->format('Y/m/d') }}&nbsp;&nbsp;{{ Carbon\Carbon::parse($s->start_time)->format('H:i') }}〜{{ Carbon\Carbon::parse($s->end_time)->format('H:i') }}</div>
                <div class="ground has-text-centered ">{{ $s->name }}</div>
                @else
                <div class="date has-text-centered ">{{ Carbon\Carbon::parse($s->date)->format('Y/m/d') }}</div>
                <div class="ground has-text-centered ">グランド未確保</div>
                @endif

                @if($s->opponent)
                <div class="opponent is-size-4 has-text-weight-bold has-text-centered">{{ $s->opponent }}様戦</div>
                @else
                <div class="opponent is-size-4 has-text-weight-bold has-text-centered">対戦相手様募集中</div>
                @endif
            </div>
        </div>
        @endforeach
    </div>
    <p>
        <a href="/schedule/" class="button is-success is-fullwidth is-size-5">全てのスケジュールはこちら</a></p>
</div>


<!-- 入団希望者／対戦相手希望チーム告知 -->
<div class="container is-fluid">
    <div class="columns">
        <div class="column has-text-centered has-background-primary has-text-white pt-50 pb-50">
            <h2 class="title is-3 has-text-white">入団を希望される方へ</h2>
            <p class="mb-20">ツインビーでは、随時選手／スタッフ（※どちらも女性歓迎）を募集しています。<br>
                募集内容や条件などの詳細はこちらをご覧ください。
            </p>
            <a class="button is-success" href="/for_join/">詳しくはこちら</a>
        </div>
        <div class="column has-text-centered has-background-info has-text-white pt-50 pb-50">
            <h2 class="title is-3 has-text-white">対戦を希望されるチーム様へ</h2>
            <p class="mb-20">ツインビーでは、随時対戦相手チーム様を募集しています。<br>
                募集内容や条件などの詳細はこちらをご覧ください。
            </p>
            <a class="button is-success" href="/match_make/">詳しくはこちら</a>
        </div>
    </div>
</div>

<!-- タイトルホルダー -->
<div class="container mb-50 mt-50">
    <h2 class="title is-3 has-text-centered">タイトルホルダー</h2>
    <div class="tabs is-centered is-boxed" id="tabs">
        <ul>
            <li class="is-active" data-tab="1"><a>打撃成績</a></li>
            <li data-tab="2"><a>投球成績</a></li>
        </ul>
    </div>
</div>

<div id="tab-content">
    <div class="is-active" data-content="1">
        <div class="container">
            <div class="columns">
                <!-- 首位打者 -->
                <div class="column">
                    <table class="table is-responsive is-bordered is-fullwidth">
                        <thead>
                            <tr class="has-background-info">
                                <th colspan="3" class="has-text-centered">打率</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($avg as $a)
                            <tr>
                                <td class="has-text-centered">{{ $a->number }}</td>
                                <td><a href="/members/{{ $a->player_id }}">{{ $a->name }}</a></td>
                                <td class="has-text-centered">{{ $a->AVG }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- 本塁打 -->
                <div class="column">
                    <table class="table is-responsive is-bordered is-fullwidth">
                        <thead>
                            <tr class="has-background-info">
                                <th colspan="3" class="has-text-centered">本塁打</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($homerun as $h)
                            <tr>
                                <td class="has-text-centered">{{ $h->number }}</td>
                                <td><a href="/members/{{ $h->player_id }}">{{ $h->name }}</a></td>
                                <td class="has-text-centered">{{ $h->homerun }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- 打点 -->
                <div class="column">
                    <table class="table is-responsive is-bordered is-fullwidth">
                        <thead>
                            <tr class="has-background-info">
                                <th colspan="3" class="has-text-centered">打点</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($rbi as $r)
                            <tr>
                                <td class="has-text-centered">{{ $r->number }}</td>
                                <td><a href="/members/{{ $r->player_id }}">{{ $r->name }}</a></td>
                                <td class="has-text-centered">{{ $r->rbi }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- 盗塁 -->
                <div class="column">
                    <table class="table is-responsive is-bordered is-fullwidth">
                        <thead>
                            <tr class="has-background-info">
                                <th colspan="3" class="has-text-centered">盗塁</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($steal as $s)
                            <tr>
                                <td class="has-text-centered">{{ $s->number }}</td>
                                <td><a href="/members/{{ $s->player_id }}">{{ $s->name }}</a></td>
                                <td class="has-text-centered">{{ $s->steal }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div data-content="2">
        <div class="container">
            <div class="columns">
                <!-- 防御率 -->
                <div class="column">
                    <table class="table is-responsive is-bordered is-fullwidth">
                        <thead>
                            <tr class="has-background-info">
                                <th colspan="3" class="has-text-centered">防御率</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($era as $e)
                            <tr>
                                <td class="has-text-centered">{{ $e->number }}</td>
                                <td><a href="/members/{{ $e->player_id }}">{{ $e->name }}</a></td>
                                <td class="has-text-centered">{{ $e->ERA }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- 勝利 -->
                <div class="column">
                    <table class="table is-responsive is-bordered is-fullwidth">
                        <thead>
                            <tr class="has-background-info">
                                <th colspan="3" class="has-text-centered">勝利</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($win as $w)
                            <tr>
                                <td class="has-text-centered">{{ $w->number }}</td>
                                <td><a href="/members/{{ $w->player_id }}">{{ $w->name }}</a></td>
                                <td class="has-text-centered">{{ $w->win }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- セーブ -->
                <div class="column">
                    <table class="table is-responsive is-bordered is-fullwidth">
                        <thead>
                            <tr class="has-background-info">
                                <th colspan="3" class="has-text-centered">セーブ</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($save as $s)
                            <tr>
                                <td class="has-text-centered">{{ $s->number }}</td>
                                <td><a href="/members/{{ $s->player_id }}">{{ $s->name }}</a></td>
                                <td class="has-text-centered">{{ $s->save }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
