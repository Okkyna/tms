@extends('layout.base')

@section('title')
入団を希望される方へ｜大阪の草野球チーム Twinbee
@endsection

@section('content')

<!-- 下層タイトル　-->
<div class="container is-fluid has-background-info pt-40 pb-40">
    <div class="container">
        <h1 class="title is-2 has-text-white">入団を希望される方へ</h1>
        <p class="subtitle is-6 has-text-white">ツインビーの募集要項をご覧いただけます。</p>
    </div>
</div>

<!-- 選手基本情報 -->
<div class="container">
    <section class="section">
        <h2 class="title is-4">ツインビーはこんなチームです！</h2>
        <div class="columns is-multiline is-marginless">
            <div class="column is-4">
                <h3 class="title is-5">目的は楽しむこと</h3>
                <p>初心者でも、経験者でも一所懸命であればOK！自分のベストプレーをして、打ち上げのビールの旨さに一緒に感動しましょう！</p>
            </div>
            <div class="column is-4">
                <h3 class="title is-5">活動は土曜日午後</h3>
                <p>原則第1・2・4・5土曜日の午後に活動しています。出席率が高いに越したことはありませんが、仕事も家庭も大事にして自身のペースで出席してください。</p>
            </div>
            <div class="column is-4">
                <h3 class="title is-5">拠点は西中島</h3>
                <p>電車でのアクセスも良く、駅からも近い淀川河川敷西中島地区での活動が中心です。<br>飲み会を楽しむために大半のメンバーは電車で参加しています。</p>
            </div>
            <div class="column is-4">
                <h3 class="title is-5">チームレベルは1程度</h3>
                <p>高校野球経験者もいますが、Twinbeeで野球を始めた人もいます。20代もいれば50代も在籍しています。最近はインド人も入団しました。経験・年齢・国籍関係なくみんなで楽しく野球しましょう！</p>
            </div>
            <div class="column is-4">
                <h3 class="title is-5">基本は全員打ち・全員守備</h3>
                <p>原則として全員が楽しめるように、全員打ち並びに必ず守備にも付いてもらうため、参加したけど出番がなかったといった心配はありません。</p>
            </div>
            <div class="column is-4">
                <h3 class="title is-5">部費は年間約10,000円</h3>
                <p>メンバー全員から徴収する部費でチームの活動を行っています。毎年予算組して部費を定めていますが、おおよそ年間10,000円程度と比較的安価な設定です。</p>
            </div>
        </div>
    </section>

    <section class="section content">
        <h2 class="title is-4">募集要項</h2>
        <table class="table is-responsive is-bordered is-fullwidth">
            <tr>
                <th class="has-background-info has-text-white">募集人員</th>
                <td>
                    <ol class="mb-20">
                        <li>選手</li>
                        <li>スタッフ・マネージャー</li>
                    </ul>
                </td>
            </tr>
            <tr>
                <th class="has-background-info has-text-white">入団条件</th>
                <td>
                    <ul>
                        <li>20歳以上の成人</li>
                        <li>チームのコンセプト／方針を理解し、チーム運営／活動全般に協力できること。</li>
                        <li><a href="/aboutus/#rule">チームのルール</a>を遵守できること</li>
                    </ul>
                    （1）選手の場合<br>
                    <ul>
                        <li>ユニフォームを購入すること（キャップ／シャツで￥14,000円程度）</li>
                        <li>部費の納入を行うこと</li>
                    </ul>
                    （2）スタッフ・マネージャー<br>
                    <ul class="mb-20">
                        <li>チーム運営において何か少しでもお手伝いをすること（スコア記録・写真撮影など）</li>
                        <li>ユニフォームの購入義務はありません。もちろん購入するのは歓迎です。</li>
                        <li>部費納入は必要ありません。</li>
                    </ul>
                </td>
            </tr>
        </table>
    </section>

    <section class="section">
            <h2 class="title is-4">入団までの流れ</h2>
            <p class="mb-20">まずは、以下お問い合わせフォームより簡単な野球経験を添えてご連絡ください。<br>
                追って、代表者より折り返しご連絡させていただきます。<br>
                まずは、数回体験いただきチームの雰囲気を実際に確認していただき、双方が入団への不安を払拭できた段階で正式に入団とさせていただいています。
            </p>
            <a href="/contact/"><button class="button is-success">お問い合わせ</button></a>
    </section>

</div>



@endsection
