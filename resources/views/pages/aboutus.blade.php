@extends('layout.base')

@section('title')
チーム紹介｜大阪の草野球チーム Twinbee
@endsection

@section('content')

<!-- 下層タイトル　-->
<div class="container is-fluid has-background-info pt-40 pb-40">
    <div class="container">
        <h1 class="title is-2 has-text-white">チーム紹介</h1>
        <p class="subtitle is-6 has-text-white">ツインビーのチーム概要をご覧いただけます。</p>
    </div>
</div>

<!-- 選手基本情報 -->
<div class="container">
    <section class="section">
        <h2 class="title is-4">チームコンセプト</h2>
        <p class="title is-3 has-text-weight-bold has-text-primary">上手下手関係なく、全員で一生懸命プレーして、全員で楽しい時間を過ごす。</p>
        <p>Twinbeeというチーム名は「Beer」と「Beef」の2つのBeeを意味しています。<br>
            チーム創設当時から変わらないことは、野球を楽しむのはもちろんですが、試合後の飲み会を欠かさず、野球をやっている時間より長いというのが日常茶飯事です（笑）。<br>
            変わったことといえば、当初は勝った日は焼き肉とビールというのが定番でしたが、10年という月日の流れの中で全体的に焼き肉だと胃もたれをするようになったところでしょうか。（笑）。
            草野球の楽しみ方には、人それぞれ様々ですが、Twinbeeでは「上手下手関係なく、全員が一生懸命プレーをして、全員で楽しい時間を過ごしたい。」という思いが基本方針です。<br>
            一生懸命プレーした後に仲間と飲むビールは格別に美味しいですよ！
        </p>
    </section>

    <section class="section content">
        <h2 class="title is-4">チーム活動概要</h2>
        <table class="table is-responsive is-bordered is-fullwidth">
            <tr>
                <th class="has-background-info has-text-white">チーム名</th>
                <td>Twinbee</td>
            </tr>
            <tr>
                <th class="has-background-info has-text-white">設立</th>
                <td>2009年12月</td>
            </tr>
            <tr>
                <th class="has-background-info has-text-white">代表者</th>
                <td><a href="/members/1">#1&nbsp;OKI</a></td>
            </tr>
            <tr>
                <th class="has-background-info has-text-white">メンバー構成</th>
                <td>選手16名、スタッフ2名&nbsp;※2019年1月時点</td>
            </tr>
            <tr>
                <th class="has-background-info has-text-white">活動拠点</th>
                <td>大阪市内
                    <ul class="mb-20">
                        <li>3月〜12月は、主に自チームで淀川河川敷西中島地区を中心にグランドを確保しています。</li>
                        <li>1月〜2月は、主に自チームで市内都市部の駅から比較的近めのグランドを確保しています。</li>
                    </ul>
                </td>
            </tr>
            <tr>
                <th class="has-background-info has-text-white">活動日</th>
                <td>
                    <ul class="mb-20">
                        <li>第1土曜日：有志にて練習</li>
                        <li>第2／4／5土曜日：練習試合／リーグ戦</li>
                        <li>原則午後の活動ですが、グランドが確保できていない場合など状況に応じ、午前中も活動します。</li>
                    </ul>
                </td>
            </tr>
            <tr>
                <th class="has-background-info has-text-white">チームレベル</th>
                <td>自己評価でLv1程度</td>
            </tr>
        </table>
    </section>

    <section id="rule" class="section content">
        <h2 class="title is-4">規約・ルール</h2>
        <p class="mb-20">チームとして細かな規約・ルールは設けていません。<br>
            但し、チームの運営や活動が円滑に行えるようチームのメンバー・対戦相手のチーム様・助っ人で参加される方などに対して、迷惑をかけない・不快な思いをさせない・約束を守るといった成人としての最低限のマナーはチームのメンバーの最低条件としています。<br>
            具体的な内容例は以下のとおりです。
            <ul>
                <li>出欠確認／部費連絡など各メンバーへの依頼があった場合、迅速に期限までに対応すること
                <li>遅刻やブッチなど
                <li>積極的にグランド整備や物品の運搬を行うこと。
            </ul>
        </p>
    </section>
</div>



@endsection
