@extends('layout.base')

@section('title')
対戦を希望されるチーム様へ｜大阪の草野球チーム Twinbee
@endsection

@section('content')

<!-- 下層タイトル　-->
<div class="container is-fluid has-background-info pt-40 pb-40">
    <div class="container">
        <h1 class="title is-2 has-text-white">対戦を希望されるチーム様へ</h1>
        <p class="subtitle is-6 has-text-white">ツインビーでは随時対戦相手様を募集しています。募集内容・条件をご覧いただけます。</p>
    </div>
</div>

<!-- 選手基本情報 -->
<div class="container">
    <section class="section">
        <h2 class="title is-4">ツインビーはこんなチームです！</h2>
        <div class="columns is-multiline is-marginless">
            <div class="column is-6">
                <h3 class="title is-5">チームレベル</h3>
                <p>自己評価でLv1程度です。初心者・経験者が混在しており、上手下手より楽しさやメンバーを公平に起用することを重視しています。<br><a href="/results/">試合結果</a>などをご覧いただき、レベル差を感じるようでしたらご遠慮いただけると助かります。</p>
            </div>
            <div class="column is-6">
                <h3 class="title is-5">活動日・時間帯</h3>
                <p>原則第1・2・4・5土曜日の午後に活動しています。活動予定は<a href="/schedule/">スケジュール</a>をご覧ください。<br>対戦相手様が決まっていない枠のご応募は大歓迎です！</p>
            </div>
            <div class="column is-6">
                <h3 class="title is-5">ルール・条件</h3>
                <p>練習試合は、全員打ち・守備交代自由・攻撃側審判・ストライクゾーンやや広めで実施させていただくことが多いです。リクエストがあれば相談させていただきますが、チームの予算の面から派遣審判を利用した練習試合はお断りしています。</p>
            </div>
            <div class="column is-6">
                <h3 class="title is-5">その他</h3>
                <p>連絡がきちんと取れる・遅刻しない・ブッチしないといった成人として最低限のマナーを持ち合わせているチーム様との対戦が最低条件です。左記条件を満たしていないと判断したチーム様からのお申し出は、お断りさせていただく場合があります。</p>
            </div>
        </div>
    </section>

    <section class="section">
            <h2 class="title is-4">対戦のお申込みについて</h2>
            <p class="mb-20">まずは、以下お問い合わせフォームよりご連絡ください。<br>
                追って、代表者より折り返しご連絡させていただきます。<br>
            </p>
            <a href="/contact/"><button class="button is-success">お問い合わせ</button></a>
    </section>

</div>



@endsection
