@extends('layout.base')

@section('title')
bulma
@endsection

@section('content')

    <!-- 下層タイトル　-->
    <div class="container is-fluid has-background-info pt-40 pb-40">
            <div class="container">
                <h1 class="title is-2 has-text-white">試合結果</h1>
                <p class="subtitle is-6 has-text-white">テキストが入ります。テキストが入ります。</p>
            </div>
        </div>

        <!-- 通算打者成績成績　-->
        <div class="container mt-50 mb-50">
            <h2 class="title is-4">通算打者成績表示</h2>
            <div class="of-scroll">
                <table class="table is-responsive is-bordered is-striped is-fullwidth">
                    <thead>
                        <tr class="has-background-info">
                            <th class="has-text-white">選手</th>
                            <th class="has-text-white">打席</th>
                            <th class="has-text-white">打数</th>
                            <th class="has-text-white">安打</th>
                            <th class="has-text-white">二塁打</th>
                            <th>三塁打</th>
                            <th>本塁打</th>
                            <th>打率</th>
                            <th>打点</th>
                            <th>得点</th>
                            <th>盗塁</th>
                            <th>四球</th>
                            <th>死球</th>
                            <th>三振</th>
                            <th>犠打</th>
                            <th>犠飛</th>
                            <th>出塁率</th>
                            <th>長打率</th>
                            <th>OPS</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>あ</td>
                            <td>う</td>
                            <td>え</td>
                            <td>お</td>
                            <td>か</td>
                            <td>あ</td>
                            <td>い</td>
                            <td>う</td>
                            <td>え</td>
                            <td>お</td>
                            <td>か</td>
                            <td>あ</td>
                            <td>い</td>
                            <td>う</td>
                            <td>え</td>
                            <td>お</td>
                            <td>か</td>
                            <td>か</td>
                            <td>か</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <!-- 戦評 -->
        <div class="container mt-50 mb-50">
            <h2 class="title is-4">中見出し</h2>
            <div class="columns is-multiline">
                <div class="column is-6">
                    <h3 class="title is-6">小見出しが入ります。</h3>
                    <p>テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。</p>
                </div>
                <div class="column is-6">
                    <h3 class="title is-6">小見出しが入ります。</h3>
                    <p>テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。</p>
                </div>
            </div>
        </div>

        <!-- タブ -->
        <div class="container mt-50 mb-50">
            <div class="tabs is-centered is-boxed">
                <ul>
                    <li class="is-active"><a>ダイジェスト</a></li>
                    <li><a>打撃成績</a></li>
                    <li><a>投球成績</a></li>
                </ul>
            </div>
        </div>



        <div class="container">
            <!-- ボタン -->
            <a class="button">.button</a>
            <a class="button is-primary">.is-primary</a>
            <a class="button is-info">.is-info</a>
            <a class="button is-success">.is-success</a>
            <a class="button is-small">.is-small</a>
            <a class="button is-medium">.is-medium</a>
            <a class="button is-large">.is-large</a>
            <a class="button is-outlined">.is-outlined</a>
            <a class="button is-inverted">.is-inverted</a>
            <a class="button is-link">.is-link</a>

            <!-- 文字色 -->
            <div>
                <span class="has-text-primary is-size-1">.has-text-primary</span>
                <span class="has-text-info is-size-3">.has-text-info</span>
            </div>

            <!--　タイルレイアウト -->
            <div class="tile is-parent">
                <div class="tile is-child box">
                    <p>タイルデザインです。</p>
                </div>
                <div class="tile is-child box">
                    <p>タイルデザインです。</p>
                </div>
            </div>
        </div>
@endsection
