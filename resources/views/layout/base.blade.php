<!doctype html>
<html lang="ja">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-7728008-5"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-7728008-5');
    </script>
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Cache-Control" content="no-cache">
    <meta http-equiv="expires" content="0" />
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta charset="UTF-8">
    <title>@yield('title')</title>
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script defer src="https://use.fontawesome.com/releases/v5.6.3/js/all.js"></script>
    <link rel="stylesheet" href="/css/app.css?v=20190128">
    <link rel="stylesheet" href="/css/custom.css?v=20190128">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="/js/function.js?v=20190128"></script>
</head>

<body>
<div id="app">

<div class="container is-fluid has-background-primary">
<header>
    <div class="container">
        <nav class="navbar is-primary" role="navigation" aria-label="main navigation">
            <div class="navbar-brand">
                <a class="navbar-item" href="/"><img src="/img/logo_twinbee.png"></a>
                <a role="button" class="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
                    <span aria-hidden="true"></span>
                    <span aria-hidden="true"></span>
                    <span aria-hidden="true"></span>
                </a>
            </div>
            <div id="navbarBasicExample" class="navbar-menu">
                <div class="navbar-start">
                    <a class="navbar-item" href="/aboutus/">チーム紹介</a>
                    <a class="navbar-item" href="/members/">選手紹介</a>
                    <a class="navbar-item" href="/results/">試合結果</a>
                    <a class="navbar-item" href="/stats/">個人成績</a>
                </div>
                <div class="navbar-end">
                    <div class="navbar-item">
                        <div class="buttons">
                            <a class="button" href="/contact/">
                                <span class="icon is-small"><i class="fas fa-envelope" aria-hidden="true"></i></span>
                                <span>問い合わせ</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    </div>
</header>
</div>

<main>
        @yield('content')
</main>


<footer class="mt-50">
    <div class="container is-fluid has-background-primary pt-50 pb-50">
        <p class="has-text-white has-text-centered is-size-6">copyright&nbsp;(c)&nbsp;Twinbee&nbsp;All&nbsp;Rights&nbsp;Reserved.</p>
    </div>
</footer>

</div>
<!-- <script src="{{ asset('js/app.js') }}"></script> -->
</body>
</html>
