@extends('layout.base')

@section('title')
個人成績｜大阪の草野球チーム Twinbee（ツインビー）
@endsection

@section('content')


<!-- 下層タイトル　-->
<div class="container is-fluid has-background-info pt-40 pb-40">
    <div class="container">
        <h1 class="title is-2 has-text-white">個人成績</h1>
        <p class="subtitle is-6 has-text-white">通算・年別などの条件で各選手の打撃成績・投球成績をご覧いただけます。</p>
    </div>
</div>

<!-- 検索エリア　-->
<div class="container">
    <section class="section">
        <div class="columns">
            <div class="column is-3 has-background-grey-light">
                <div class="search_title_wrapper">
                    <h2 class="title is-4 search_title has-text-white">絞り込み検索</h2>
                </div>
            </div>
            <div class="column has-background-white-ter">
                <form action="{{ url('/stats') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="field">
                        <label class="label">年度選択</label>
                    </div>
                    <div class="control">
                        <div class="select is-rounded">
                            <select name="year_search" size="1">
                                <option disabled selected>選択してください。</option>
                                <option value="all">全て</option>
                                <option value="2020">2020年</option>
                                <option value="2019">2019年</option>
                                <option value="2018">2018年</option>
                                <option value="2017">2017年</option>
                                <option value="2016">2016年</option>
                                <option value="2015">2015年</option>
                                <option value="2014">2014年</option>
                                <option value="2013">2013年</option>
                                <option value="2012">2012年</option>
                                <option value="2011">2011年</option>
                                <option value="2010">2010年</option>
                            </select>
                        </div>
                    </div>
                    <div class="field mt-20">
                        <div class="control">
                            <button class="button is-success has-text-weight-bold" type="submit" name="search">検索</button>
                        </div>
                    </div>
                </form>
                <p class="mt-20 has-text-primary">※2017年以前のデータは個人の試合が正しく表示されません。</p>
            </div>
        </div>
    </section>
</div>

<div class="container">
    <h2 class="title is-3">
        @isset($title)
            {{ $title }}成績
        @endisset
    </h2>
    <h3 class="title is-5">打撃成績</h3>
    <div class="of-scroll mb-50">
        <table class="table is-responsive is-bordered is-striped is-fullwidth">
            <thead>
                <tr class="has-background-info">
                    <th class="has-text-centered">背番号</th>
                    <th class="has-text-centered">選手</th>
                    <th class="has-text-centered">試合数</th>
                    <th class="has-text-centered">打席</th>
                    <th class="has-text-centered">打数</th>
                    <th class="has-text-centered">安打</th>
                    <th class="has-text-centered">二塁打</th>
                    <th class="has-text-centered">三塁打</th>
                    <th class="has-text-centered">本塁打</th>
                    <th class="has-text-centered">打率</th>
                    <th class="has-text-centered">打点</th>
                    <th class="has-text-centered">得点</th>
                    <th class="has-text-centered">盗塁</th>
                    <th class="has-text-centered">四球</th>
                    <th class="has-text-centered">死球</th>
                    <th class="has-text-centered">三振</th>
                    <th class="has-text-centered">犠打</th>
                    <th class="has-text-centered">犠飛</th>
                    <th class="has-text-centered">出塁率</th>
                    <th class="has-text-centered">長打率</th>
                    <th class="has-text-centered">OPS</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($batting_stats as $b)
                <tr>
                    <td class="has-text-centered">{{ $b->number }}</td>
                    <td><a href="/members/{{ $b->player_id }}">{{ $b->name }}</a></td>
                    <td class="has-text-centered">{{ $b->games }}</td>
                    <td class="has-text-centered">{{ $b->at_bat }}</td>
                    <td class="has-text-centered">{{ $b->at_stroke }}</td>
                    <td class="has-text-centered">{{ $b->hits }}</td>
                    <td class="has-text-centered">{{ $b->doubles }}</td>
                    <td class="has-text-centered">{{ $b->triples }}</td>
                    <td class="has-text-centered">{{ $b->homerun }}</td>
                    <td class="has-text-centered">{{ $b->AVG }}</td>
                    <td class="has-text-centered">{{ $b->rbi }}</td>
                    <td class="has-text-centered">{{ $b->run }}</td>
                    <td class="has-text-centered">{{ $b->steal }}</td>
                    <td class="has-text-centered">{{ $b->walk }}</td>
                    <td class="has-text-centered">{{ $b->plunked }}</td>
                    <td class="has-text-centered">{{ $b->strike_out }}</td>
                    <td class="has-text-centered">{{ $b->sacrifice_bunt }}</td>
                    <td class="has-text-centered">{{ $b->sacrifice_fly }}</td>
                    <td class="has-text-centered">{{ $b->OBP }}</td>
                    <td class="has-text-centered">{{ $b->SLG }}</td>
                    <td class="has-text-centered">{{ $b->OPS }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <h3 class="title is-5">投球成績</h3>
    <div class="of-scroll mb-50">
        <table class="table is-responsive is-bordered is-striped is-fullwidth">
            <thead>
                <tr class="has-background-info">
                    <th class="has-text-centered">背番号</th>
                    <th class="has-text-centered">選手名</th>
                    <th class="has-text-centered">試合数</th>
                    <th class="has-text-centered">投球回</th>
                    <th class="has-text-centered">失点</th>
                    <th class="has-text-centered">自責点</th>
                    <th class="has-text-centered">防御率</th>
                    <th class="has-text-centered">勝</th>
                    <th class="has-text-centered">負</th>
                    <th class="has-text-centered">H</th>
                    <th class="has-text-centered">S</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($pitching_stats as $p)
                <tr>
                    <td class="has-text-centered">{{ $p->number }}</td>
                    <td><a href="/members/{{ $p->player_id }}">{{ $p->name }}</a></td>
                    <td class="has-text-centered">{{ $p->games }}</td>
                    <td class="has-text-centered">{{ $p->innings }}@if ($p->fraction_innings > 0)&nbsp;{{ $p->fraction_innings }}/3 @endif</td>
                    <td class="has-text-centered">{{ $p->run }}</td>
                    <td class="has-text-centered">{{ $p->earned_run }}</td>
                    <td class="has-text-centered">{{ $p->ERA }}</td>
                    <td class="has-text-centered">{{ $p->win }}</td>
                    <td class="has-text-centered">{{ $p->lose }}</td>
                    <td class="has-text-centered">{{ $p->hold }}</td>
                    <td class="has-text-centered">{{ $p->save }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
