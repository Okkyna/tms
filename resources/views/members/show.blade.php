@extends('layout.base')

@section('title')
{{ $member->name }}｜大阪の草野球チーム Twinbee
@endsection

@section('content')

<!-- 下層タイトル　-->
<div class="container is-fluid has-background-info pt-40 pb-40">
    <div class="container">
        <h1 class="title is-2 has-text-white">選手紹介</h1>
        <p class="subtitle is-6 has-text-white">ツインビーに在籍する選手・スタッフをご覧いただけます。</p>
    </div>
</div>

<!-- 選手基本情報 -->
<div class="container">
    <section class="section">
        <div class="columns">
            <div class="column">
                <img src="/img/members/{{ $member->photo_main }}">
            </div>
            <div class="column">
                <p  class="title is-1 has-text-centered has-text-info">{{ $member->number }}</p>
                <h1 class="title is-1 has-text-centered has-text-info">{{ $member->name }}</h1>
                <p class="title is-5">{{ $member->copy }}</p>
                <p class="mb-20">{{ $member->text }}</p>
                <table class="table is-responsive is-bordered is-fullwidth">
                    <tbody>
                        <tr>
                            <th class="has-background-info has-text-white">メンバー区分</th>
                            <td>
                                @if($member->player_category == 1)選手
                                @elseif($member->player_category == 2)スタッフ
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th class="has-background-info has-text-white">役割</th>
                            <td>
                                @isset($member->role){{ $member->role }}@endisset
                            </td>
                        </tr>
                        <tr>
                            <th class="has-background-info has-text-white">入団年月</th>
                            <td>{{ Carbon\Carbon::parse($member->join_at)->format('Y年m月') }}</td>
                        </tr>
                        <tr>
                            <th class="has-background-info has-text-white">獲得タイトル</th>
                            <td>{{ $member->acquired_titles }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</div>

<!-- タブナビゲーション -->
<div class="container mb-20">
    <h2 class="title is-3 has-text-centered">個人通算成績</h2>
    <div class="tabs is-centered is-boxed" id="tabs">
        <ul>
            <li class="is-active" data-tab="1"><a>打撃成績</a></li>
            <li data-tab="2"><a>投球成績</a></li>
        </ul>
    </div>
</div>

<!-- タブコンテンツ -->
<div id="tab-content">
    <!-- 打撃成績　-->
    <div class="is-active" data-content="1">
        <div class="container pt-10">
            <div class="of-scroll">
                <table class="table is-responsive is-bordered is-fullwidth">
                    <thead>
                        <tr class="has-background-info">
                            <th class="has-text-centered">年度</th>
                            <th class="has-text-centered">試合数</th>
                            <th class="has-text-centered">打席</th>
                            <th class="has-text-centered">打数</th>
                            <th class="has-text-centered">安打</th>
                            <th class="has-text-centered">二塁打</th>
                            <th class="has-text-centered">三塁打</th>
                            <th class="has-text-centered">本塁打</th>
                            <th class="has-text-centered">打率</th>
                            <th class="has-text-centered">打点</th>
                            <th class="has-text-centered">得点</th>
                            <th class="has-text-centered">盗塁</th>
                            <th class="has-text-centered">四球</th>
                            <th class="has-text-centered">死球</th>
                            <th class="has-text-centered">三振</th>
                            <th class="has-text-centered">犠打</th>
                            <th class="has-text-centered">犠飛</th>
                            <td class="has-text-centered">出塁率</td>
                            <td class="has-text-centered">長打率</td>
                            <td class="has-text-centered">OPS</td>
                        </tr>
                    </thead>
                    <tbody>
                        @if($batting_data_2011[0]->games > 0)
                        <tr>
                            <td class="has-text-centered">2011</td>
                            <td class="has-text-centered">{{ $batting_data_2011[0]->games }}</td>
                            <td class="has-text-centered">{{ $batting_data_2011[0]->at_bat }}</td>
                            <td class="has-text-centered">{{ $batting_data_2011[0]->at_stroke }}</td>
                            <td class="has-text-centered">{{ $batting_data_2011[0]->hits }}</td>
                            <td class="has-text-centered">{{ $batting_data_2011[0]->doubles }}</td>
                            <td class="has-text-centered">{{ $batting_data_2011[0]->triples }}</td>
                            <td class="has-text-centered">{{ $batting_data_2011[0]->homerun }}</td>
                            <td class="has-text-centered">{{ $batting_data_2011[0]->AVG }}</td>
                            <td class="has-text-centered">{{ $batting_data_2011[0]->rbi }}</td>
                            <td class="has-text-centered">{{ $batting_data_2011[0]->run }}</td>
                            <td class="has-text-centered">{{ $batting_data_2011[0]->steal }}</td>
                            <td class="has-text-centered">{{ $batting_data_2011[0]->walk }}</td>
                            <td class="has-text-centered">{{ $batting_data_2011[0]->plunked }}</td>
                            <td class="has-text-centered">{{ $batting_data_2011[0]->strike_out }}</td>
                            <td class="has-text-centered">{{ $batting_data_2011[0]->sacrifice_bunt }}</td>
                            <td class="has-text-centered">{{ $batting_data_2011[0]->sacrifice_fly }}</td>
                            <td class="has-text-centered">{{ $batting_data_2011[0]->OBP }}</td>
                            <td class="has-text-centered">{{ $batting_data_2011[0]->SLG }}</td>
                            <td class="has-text-centered">{{ $batting_data_2011[0]->OPS }}</td>
                        </tr>
                        @endif
                        @if($batting_data_2012[0]->games > 0)
                        <tr>
                            <td class="has-text-centered">2012</td>
                            <td class="has-text-centered">{{ $batting_data_2012[0]->games }}</td>
                            <td class="has-text-centered">{{ $batting_data_2012[0]->at_bat }}</td>
                            <td class="has-text-centered">{{ $batting_data_2012[0]->at_stroke }}</td>
                            <td class="has-text-centered">{{ $batting_data_2012[0]->hits }}</td>
                            <td class="has-text-centered">{{ $batting_data_2012[0]->doubles }}</td>
                            <td class="has-text-centered">{{ $batting_data_2012[0]->triples }}</td>
                            <td class="has-text-centered">{{ $batting_data_2012[0]->homerun }}</td>
                            <td class="has-text-centered">{{ $batting_data_2012[0]->AVG }}</td>
                            <td class="has-text-centered">{{ $batting_data_2012[0]->rbi }}</td>
                            <td class="has-text-centered">{{ $batting_data_2012[0]->run }}</td>
                            <td class="has-text-centered">{{ $batting_data_2012[0]->steal }}</td>
                            <td class="has-text-centered">{{ $batting_data_2012[0]->walk }}</td>
                            <td class="has-text-centered">{{ $batting_data_2012[0]->plunked }}</td>
                            <td class="has-text-centered">{{ $batting_data_2012[0]->strike_out }}</td>
                            <td class="has-text-centered">{{ $batting_data_2012[0]->sacrifice_bunt }}</td>
                            <td class="has-text-centered">{{ $batting_data_2012[0]->sacrifice_fly }}</td>
                            <td class="has-text-centered">{{ $batting_data_2012[0]->OBP }}</td>
                            <td class="has-text-centered">{{ $batting_data_2012[0]->SLG }}</td>
                            <td class="has-text-centered">{{ $batting_data_2012[0]->OPS }}</td>
                        </tr>
                        @endif
                        @if($batting_data_2013[0]->games > 0)
                        <tr>
                            <td class="has-text-centered">2013</td>
                            <td class="has-text-centered">{{ $batting_data_2013[0]->games }}</td>
                            <td class="has-text-centered">{{ $batting_data_2013[0]->at_bat }}</td>
                            <td class="has-text-centered">{{ $batting_data_2013[0]->at_stroke }}</td>
                            <td class="has-text-centered">{{ $batting_data_2013[0]->hits }}</td>
                            <td class="has-text-centered">{{ $batting_data_2013[0]->doubles }}</td>
                            <td class="has-text-centered">{{ $batting_data_2013[0]->triples }}</td>
                            <td class="has-text-centered">{{ $batting_data_2013[0]->homerun }}</td>
                            <td class="has-text-centered">{{ $batting_data_2013[0]->AVG }}</td>
                            <td class="has-text-centered">{{ $batting_data_2013[0]->rbi }}</td>
                            <td class="has-text-centered">{{ $batting_data_2013[0]->run }}</td>
                            <td class="has-text-centered">{{ $batting_data_2013[0]->steal }}</td>
                            <td class="has-text-centered">{{ $batting_data_2013[0]->walk }}</td>
                            <td class="has-text-centered">{{ $batting_data_2013[0]->plunked }}</td>
                            <td class="has-text-centered">{{ $batting_data_2013[0]->strike_out }}</td>
                            <td class="has-text-centered">{{ $batting_data_2013[0]->sacrifice_bunt }}</td>
                            <td class="has-text-centered">{{ $batting_data_2013[0]->sacrifice_fly }}</td>
                            <td class="has-text-centered">{{ $batting_data_2013[0]->OBP }}</td>
                            <td class="has-text-centered">{{ $batting_data_2013[0]->SLG }}</td>
                            <td class="has-text-centered">{{ $batting_data_2013[0]->OPS }}</td>
                        </tr>
                        @endif
                        @if($batting_data_2014[0]->games > 0)
                        <tr>
                            <td class="has-text-centered">2014</td>
                            <td class="has-text-centered">{{ $batting_data_2014[0]->games }}</td>
                            <td class="has-text-centered">{{ $batting_data_2014[0]->at_bat }}</td>
                            <td class="has-text-centered">{{ $batting_data_2014[0]->at_stroke }}</td>
                            <td class="has-text-centered">{{ $batting_data_2014[0]->hits }}</td>
                            <td class="has-text-centered">{{ $batting_data_2014[0]->doubles }}</td>
                            <td class="has-text-centered">{{ $batting_data_2014[0]->triples }}</td>
                            <td class="has-text-centered">{{ $batting_data_2014[0]->homerun }}</td>
                            <td class="has-text-centered">{{ $batting_data_2014[0]->AVG }}</td>
                            <td class="has-text-centered">{{ $batting_data_2014[0]->rbi }}</td>
                            <td class="has-text-centered">{{ $batting_data_2014[0]->run }}</td>
                            <td class="has-text-centered">{{ $batting_data_2014[0]->steal }}</td>
                            <td class="has-text-centered">{{ $batting_data_2014[0]->walk }}</td>
                            <td class="has-text-centered">{{ $batting_data_2014[0]->plunked }}</td>
                            <td class="has-text-centered">{{ $batting_data_2014[0]->strike_out }}</td>
                            <td class="has-text-centered">{{ $batting_data_2014[0]->sacrifice_bunt }}</td>
                            <td class="has-text-centered">{{ $batting_data_2014[0]->sacrifice_fly }}</td>
                            <td class="has-text-centered">{{ $batting_data_2014[0]->OBP }}</td>
                            <td class="has-text-centered">{{ $batting_data_2014[0]->SLG }}</td>
                            <td class="has-text-centered">{{ $batting_data_2014[0]->OPS }}</td>
                        </tr>
                        @endif
                        @if($batting_data_2015[0]->games > 0)
                        <tr>
                            <td class="has-text-centered">2015</td>
                            <td class="has-text-centered">{{ $batting_data_2015[0]->games }}</td>
                            <td class="has-text-centered">{{ $batting_data_2015[0]->at_bat }}</td>
                            <td class="has-text-centered">{{ $batting_data_2015[0]->at_stroke }}</td>
                            <td class="has-text-centered">{{ $batting_data_2015[0]->hits }}</td>
                            <td class="has-text-centered">{{ $batting_data_2015[0]->doubles }}</td>
                            <td class="has-text-centered">{{ $batting_data_2015[0]->triples }}</td>
                            <td class="has-text-centered">{{ $batting_data_2015[0]->homerun }}</td>
                            <td class="has-text-centered">{{ $batting_data_2015[0]->AVG }}</td>
                            <td class="has-text-centered">{{ $batting_data_2015[0]->rbi }}</td>
                            <td class="has-text-centered">{{ $batting_data_2015[0]->run }}</td>
                            <td class="has-text-centered">{{ $batting_data_2015[0]->steal }}</td>
                            <td class="has-text-centered">{{ $batting_data_2015[0]->walk }}</td>
                            <td class="has-text-centered">{{ $batting_data_2015[0]->plunked }}</td>
                            <td class="has-text-centered">{{ $batting_data_2015[0]->strike_out }}</td>
                            <td class="has-text-centered">{{ $batting_data_2015[0]->sacrifice_bunt }}</td>
                            <td class="has-text-centered">{{ $batting_data_2015[0]->sacrifice_fly }}</td>
                            <td class="has-text-centered">{{ $batting_data_2015[0]->OBP }}</td>
                            <td class="has-text-centered">{{ $batting_data_2015[0]->SLG }}</td>
                            <td class="has-text-centered">{{ $batting_data_2015[0]->OPS }}</td>
                        </tr>
                        @endif
                        @if($batting_data_2016[0]->games > 0)
                        <tr>
                            <td>2016</td>
                            <td class="has-text-centered">{{ $batting_data_2016[0]->games }}</td>
                            <td class="has-text-centered">{{ $batting_data_2016[0]->at_bat }}</td>
                            <td class="has-text-centered">{{ $batting_data_2016[0]->at_stroke }}</td>
                            <td class="has-text-centered">{{ $batting_data_2016[0]->hits }}</td>
                            <td class="has-text-centered">{{ $batting_data_2016[0]->doubles }}</td>
                            <td class="has-text-centered">{{ $batting_data_2016[0]->triples }}</td>
                            <td class="has-text-centered">{{ $batting_data_2016[0]->homerun }}</td>
                            <td class="has-text-centered">{{ $batting_data_2016[0]->AVG }}</td>
                            <td class="has-text-centered">{{ $batting_data_2016[0]->rbi }}</td>
                            <td class="has-text-centered">{{ $batting_data_2016[0]->run }}</td>
                            <td class="has-text-centered">{{ $batting_data_2016[0]->steal }}</td>
                            <td class="has-text-centered">{{ $batting_data_2016[0]->walk }}</td>
                            <td class="has-text-centered">{{ $batting_data_2016[0]->plunked }}</td>
                            <td class="has-text-centered">{{ $batting_data_2016[0]->strike_out }}</td>
                            <td class="has-text-centered">{{ $batting_data_2016[0]->sacrifice_bunt }}</td>
                            <td class="has-text-centered">{{ $batting_data_2016[0]->sacrifice_fly }}</td>
                            <td class="has-text-centered">{{ $batting_data_2016[0]->OBP }}</td>
                            <td class="has-text-centered">{{ $batting_data_2016[0]->SLG }}</td>
                            <td class="has-text-centered">{{ $batting_data_2016[0]->OPS }}</td>
                        </tr>
                        @endif
                        @if($batting_data_2017[0]->games > 0)
                        <tr>
                            <td class="has-text-centered">2017</td>
                            <td class="has-text-centered">{{ $batting_data_2017[0]->games }}</td>
                            <td class="has-text-centered">{{ $batting_data_2017[0]->at_bat }}</td>
                            <td class="has-text-centered">{{ $batting_data_2017[0]->at_stroke }}</td>
                            <td class="has-text-centered">{{ $batting_data_2017[0]->hits }}</td>
                            <td class="has-text-centered">{{ $batting_data_2017[0]->doubles }}</td>
                            <td class="has-text-centered">{{ $batting_data_2017[0]->triples }}</td>
                            <td class="has-text-centered">{{ $batting_data_2017[0]->homerun }}</td>
                            <td class="has-text-centered">{{ $batting_data_2017[0]->AVG }}</td>
                            <td class="has-text-centered">{{ $batting_data_2017[0]->rbi }}</td>
                            <td class="has-text-centered">{{ $batting_data_2017[0]->run }}</td>
                            <td class="has-text-centered">{{ $batting_data_2017[0]->steal }}</td>
                            <td class="has-text-centered">{{ $batting_data_2017[0]->walk }}</td>
                            <td class="has-text-centered">{{ $batting_data_2017[0]->plunked }}</td>
                            <td class="has-text-centered">{{ $batting_data_2017[0]->strike_out }}</td>
                            <td class="has-text-centered">{{ $batting_data_2017[0]->sacrifice_bunt }}</td>
                            <td class="has-text-centered">{{ $batting_data_2017[0]->sacrifice_fly }}</td>
                            <td class="has-text-centered">{{ $batting_data_2017[0]->OBP }}</td>
                            <td class="has-text-centered">{{ $batting_data_2017[0]->SLG }}</td>
                            <td class="has-text-centered">{{ $batting_data_2017[0]->OPS }}</td>
                        </tr>
                        @endif
                        @if($batting_data_2018[0]->games > 0)
                        <tr>
                            <td class="has-text-centered">2018</td>
                            <td class="has-text-centered">{{ $batting_data_2018[0]->games }}</td>
                            <td class="has-text-centered">{{ $batting_data_2018[0]->at_bat }}</td>
                            <td class="has-text-centered">{{ $batting_data_2018[0]->at_stroke }}</td>
                            <td class="has-text-centered">{{ $batting_data_2018[0]->hits }}</td>
                            <td class="has-text-centered">{{ $batting_data_2018[0]->doubles }}</td>
                            <td class="has-text-centered">{{ $batting_data_2018[0]->triples }}</td>
                            <td class="has-text-centered">{{ $batting_data_2018[0]->homerun }}</td>
                            <td class="has-text-centered">{{ $batting_data_2018[0]->AVG }}</td>
                            <td class="has-text-centered">{{ $batting_data_2018[0]->rbi }}</td>
                            <td class="has-text-centered">{{ $batting_data_2018[0]->run }}</td>
                            <td class="has-text-centered">{{ $batting_data_2018[0]->steal }}</td>
                            <td class="has-text-centered">{{ $batting_data_2018[0]->walk }}</td>
                            <td class="has-text-centered">{{ $batting_data_2018[0]->plunked }}</td>
                            <td class="has-text-centered">{{ $batting_data_2018[0]->strike_out }}</td>
                            <td class="has-text-centered">{{ $batting_data_2018[0]->sacrifice_bunt }}</td>
                            <td class="has-text-centered">{{ $batting_data_2018[0]->sacrifice_fly }}</td>
                            <td class="has-text-centered">{{ $batting_data_2018[0]->OBP }}</td>
                            <td class="has-text-centered">{{ $batting_data_2018[0]->SLG }}</td>
                            <td class="has-text-centered">{{ $batting_data_2018[0]->OPS }}</td>
                        </tr>
                        @endif
                        @if($batting_data_2019[0]->games > 0)
                        <tr>
                            <td class="has-text-centered">2019</td>
                            <td class="has-text-centered">{{ $batting_data_2019[0]->games }}</td>
                            <td class="has-text-centered">{{ $batting_data_2019[0]->at_bat }}</td>
                            <td class="has-text-centered">{{ $batting_data_2019[0]->at_stroke }}</td>
                            <td class="has-text-centered">{{ $batting_data_2019[0]->hits }}</td>
                            <td class="has-text-centered">{{ $batting_data_2019[0]->doubles }}</td>
                            <td class="has-text-centered">{{ $batting_data_2019[0]->triples }}</td>
                            <td class="has-text-centered">{{ $batting_data_2019[0]->homerun }}</td>
                            <td class="has-text-centered">{{ $batting_data_2019[0]->AVG }}</td>
                            <td class="has-text-centered">{{ $batting_data_2019[0]->rbi }}</td>
                            <td class="has-text-centered">{{ $batting_data_2019[0]->run }}</td>
                            <td class="has-text-centered">{{ $batting_data_2019[0]->steal }}</td>
                            <td class="has-text-centered">{{ $batting_data_2019[0]->walk }}</td>
                            <td class="has-text-centered">{{ $batting_data_2019[0]->plunked }}</td>
                            <td class="has-text-centered">{{ $batting_data_2019[0]->strike_out }}</td>
                            <td class="has-text-centered">{{ $batting_data_2019[0]->sacrifice_bunt }}</td>
                            <td class="has-text-centered">{{ $batting_data_2019[0]->sacrifice_fly }}</td>
                            <td class="has-text-centered">{{ $batting_data_2019[0]->OBP }}</td>
                            <td class="has-text-centered">{{ $batting_data_2019[0]->SLG }}</td>
                            <td class="has-text-centered">{{ $batting_data_2019[0]->OPS }}</td>
                        </tr>
                        @endif
                        @if($batting_data_2020[0]->games > 0)
                        <tr>
                            <td class="has-text-centered">2020</td>
                            <td class="has-text-centered">{{ $batting_data_2020[0]->games }}</td>
                            <td class="has-text-centered">{{ $batting_data_2020[0]->at_bat }}</td>
                            <td class="has-text-centered">{{ $batting_data_2020[0]->at_stroke }}</td>
                            <td class="has-text-centered">{{ $batting_data_2020[0]->hits }}</td>
                            <td class="has-text-centered">{{ $batting_data_2020[0]->doubles }}</td>
                            <td class="has-text-centered">{{ $batting_data_2020[0]->triples }}</td>
                            <td class="has-text-centered">{{ $batting_data_2020[0]->homerun }}</td>
                            <td class="has-text-centered">{{ $batting_data_2020[0]->AVG }}</td>
                            <td class="has-text-centered">{{ $batting_data_2020[0]->rbi }}</td>
                            <td class="has-text-centered">{{ $batting_data_2020[0]->run }}</td>
                            <td class="has-text-centered">{{ $batting_data_2020[0]->steal }}</td>
                            <td class="has-text-centered">{{ $batting_data_2020[0]->walk }}</td>
                            <td class="has-text-centered">{{ $batting_data_2020[0]->plunked }}</td>
                            <td class="has-text-centered">{{ $batting_data_2020[0]->strike_out }}</td>
                            <td class="has-text-centered">{{ $batting_data_2020[0]->sacrifice_bunt }}</td>
                            <td class="has-text-centered">{{ $batting_data_2020[0]->sacrifice_fly }}</td>
                            <td class="has-text-centered">{{ $batting_data_2020[0]->OBP }}</td>
                            <td class="has-text-centered">{{ $batting_data_2020[0]->SLG }}</td>
                            <td class="has-text-centered">{{ $batting_data_2020[0]->OPS }}</td>
                        </tr>
                        @endif
                        @if($batting_data_2021[0]->games > 0)
                        <tr>
                            <td class="has-text-centered">2021</td>
                            <td class="has-text-centered">{{ $batting_data_2021[0]->games }}</td>
                            <td class="has-text-centered">{{ $batting_data_2021[0]->at_bat }}</td>
                            <td class="has-text-centered">{{ $batting_data_2021[0]->at_stroke }}</td>
                            <td class="has-text-centered">{{ $batting_data_2021[0]->hits }}</td>
                            <td class="has-text-centered">{{ $batting_data_2021[0]->doubles }}</td>
                            <td class="has-text-centered">{{ $batting_data_2021[0]->triples }}</td>
                            <td class="has-text-centered">{{ $batting_data_2021[0]->homerun }}</td>
                            <td class="has-text-centered">{{ $batting_data_2021[0]->AVG }}</td>
                            <td class="has-text-centered">{{ $batting_data_2021[0]->rbi }}</td>
                            <td class="has-text-centered">{{ $batting_data_2021[0]->run }}</td>
                            <td class="has-text-centered">{{ $batting_data_2021[0]->steal }}</td>
                            <td class="has-text-centered">{{ $batting_data_2021[0]->walk }}</td>
                            <td class="has-text-centered">{{ $batting_data_2021[0]->plunked }}</td>
                            <td class="has-text-centered">{{ $batting_data_2021[0]->strike_out }}</td>
                            <td class="has-text-centered">{{ $batting_data_2021[0]->sacrifice_bunt }}</td>
                            <td class="has-text-centered">{{ $batting_data_2021[0]->sacrifice_fly }}</td>
                            <td class="has-text-centered">{{ $batting_data_2021[0]->OBP }}</td>
                            <td class="has-text-centered">{{ $batting_data_2021[0]->SLG }}</td>
                            <td class="has-text-centered">{{ $batting_data_2021[0]->OPS }}</td>
                        </tr>
                        @endif
                        @if($batting_data_all[0]->games > 0)
                        <tr class="has-background-light has-text-weight-semibold">
                            <td class="has-text-centered">通算</td>
                            <td class="has-text-centered">{{ $batting_data_all[0]->games }}</td>
                            <td class="has-text-centered">{{ $batting_data_all[0]->at_bat }}</td>
                            <td class="has-text-centered">{{ $batting_data_all[0]->at_stroke }}</td>
                            <td class="has-text-centered">{{ $batting_data_all[0]->hits }}</td>
                            <td class="has-text-centered">{{ $batting_data_all[0]->doubles }}</td>
                            <td class="has-text-centered">{{ $batting_data_all[0]->triples }}</td>
                            <td class="has-text-centered">{{ $batting_data_all[0]->homerun }}</td>
                            <td class="has-text-centered">{{ $batting_data_all[0]->AVG }}</td>
                            <td class="has-text-centered">{{ $batting_data_all[0]->rbi }}</td>
                            <td class="has-text-centered">{{ $batting_data_all[0]->run }}</td>
                            <td class="has-text-centered">{{ $batting_data_all[0]->steal }}</td>
                            <td class="has-text-centered">{{ $batting_data_all[0]->walk }}</td>
                            <td class="has-text-centered">{{ $batting_data_all[0]->plunked }}</td>
                            <td class="has-text-centered">{{ $batting_data_all[0]->strike_out }}</td>
                            <td class="has-text-centered">{{ $batting_data_all[0]->sacrifice_bunt }}</td>
                            <td class="has-text-centered">{{ $batting_data_all[0]->sacrifice_fly }}</td>
                            <td class="has-text-centered">{{ $batting_data_all[0]->OBP }}</td>
                            <td class="has-text-centered">{{ $batting_data_all[0]->SLG }}</td>
                            <td class="has-text-centered">{{ $batting_data_all[0]->OPS }}</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <!-- 投球成績　-->
    <div data-content="2">
        <div class="container pt-10">
            <div class="of-scroll">
                <table class="table is-responsive is-bordered is-fullwidth">
                    <thead>
                        <tr class="has-background-info">
                            <th class="has-text-centered">年度</th>
                            <th class="has-text-centered">試合数</th>
                            <th class="has-text-centered">投球回</th>
                            <th class="has-text-centered">打者数</th>
                            <th class="has-text-centered">被安打</th>
                            <th class="has-text-centered">被本塁打</th>
                            <th class="has-text-centered">奪三振</th>
                            <th class="has-text-centered">与四球</th>
                            <th class="has-text-centered">与死球</th>
                            <th class="has-text-centered">失点</th>
                            <th class="has-text-centered">自責点</th>
                            <th class="has-text-centered">防御率</th>
                            <th class="has-text-centered">完投</th>
                            <th class="has-text-centered">勝</th>
                            <th class="has-text-centered">負</th>
                            <th class="has-text-centered">H</th>
                            <th class="has-text-centered">S</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($pitching_data_2011[0]->games > 0)
                        <tr>
                            <td class="has-text-centered">2011</td>
                            <td class="has-text-centered">{{ $pitching_data_2011[0]->games }}</td>
                            <td class="has-text-centered">
                                {{ $pitching_data_2011[0]->innings }}
                                @if($pitching_data_2011[0]->fraction_innings > 0)
                                    {{ $pitching_data_2011[0]->fraction_innings }}/3
                                @endif</td>
                            <td class="has-text-centered">{{ $pitching_data_2011[0]->at_bat }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2011[0]->hits }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2011[0]->homerun }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2011[0]->strike_out }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2011[0]->walk }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2011[0]->plunked }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2011[0]->run }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2011[0]->earned_run }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2011[0]->ERA }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2011[0]->throw_full_innings }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2011[0]->win }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2011[0]->lose }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2011[0]->hold }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2011[0]->save }}</td>
                        </tr>
                        @endif
                        @if($pitching_data_2012[0]->games > 0)
                        <tr>
                            <td class="has-text-centered">2012</td>
                            <td class="has-text-centered">{{ $pitching_data_2012[0]->games }}</td>
                            <td class="has-text-centered">
                                {{ $pitching_data_2012[0]->innings }}
                                @if($pitching_data_2012[0]->fraction_innings > 0)
                                    {{ $pitching_data_2012[0]->fraction_innings }}/3
                                @endif</td>
                            <td class="has-text-centered">{{ $pitching_data_2012[0]->at_bat }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2012[0]->hits }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2012[0]->homerun }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2012[0]->strike_out }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2012[0]->walk }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2012[0]->plunked }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2012[0]->run }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2012[0]->earned_run }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2012[0]->ERA }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2012[0]->throw_full_innings }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2012[0]->win }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2012[0]->lose }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2012[0]->hold }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2012[0]->save }}</td>
                        </tr>
                        @endif
                        @if($pitching_data_2013[0]->games > 0)
                        <tr>
                            <td class="has-text-centered">2013</td>
                            <td class="has-text-centered">{{ $pitching_data_2013[0]->games }}</td>
                            <td class="has-text-centered">
                                {{ $pitching_data_2013[0]->innings }}
                                @if($pitching_data_2013[0]->fraction_innings > 0)
                                    {{ $pitching_data_2013[0]->fraction_innings }}/3
                                @endif</td>
                            <td class="has-text-centered">{{ $pitching_data_2013[0]->at_bat }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2013[0]->hits }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2013[0]->homerun }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2013[0]->strike_out }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2013[0]->walk }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2013[0]->plunked }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2013[0]->run }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2013[0]->earned_run }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2013[0]->ERA }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2013[0]->throw_full_innings }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2013[0]->win }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2013[0]->lose }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2013[0]->hold }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2013[0]->save }}</td>
                        </tr>
                        @endif
                        @if($pitching_data_2014[0]->games > 0)
                        <tr>
                            <td class="has-text-centered">2014</td>
                            <td class="has-text-centered">{{ $pitching_data_2014[0]->games }}</td>
                            <td class="has-text-centered">
                                {{ $pitching_data_2014[0]->innings }}
                                @if($pitching_data_2014[0]->fraction_innings > 0)
                                    {{ $pitching_data_2014[0]->fraction_innings }}/3
                                @endif</td>
                            <td class="has-text-centered">{{ $pitching_data_2014[0]->at_bat }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2014[0]->hits }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2014[0]->homerun }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2014[0]->strike_out }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2014[0]->walk }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2014[0]->plunked }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2014[0]->run }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2014[0]->earned_run }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2014[0]->ERA }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2014[0]->throw_full_innings }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2014[0]->win }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2014[0]->lose }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2014[0]->hold }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2014[0]->save }}</td>
                        </tr>
                        @endif
                        @if($pitching_data_2015[0]->games > 0)
                        <tr>
                            <td class="has-text-centered">2015</td>
                            <td class="has-text-centered">{{ $pitching_data_2015[0]->games }}</td>
                            <td class="has-text-centered">
                                {{ $pitching_data_2015[0]->innings }}
                                @if($pitching_data_2015[0]->fraction_innings > 0)
                                    {{ $pitching_data_2015[0]->fraction_innings }}/3
                                @endif</td>
                            <td class="has-text-centered">{{ $pitching_data_2015[0]->at_bat }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2015[0]->hits }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2015[0]->homerun }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2015[0]->strike_out }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2015[0]->walk }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2015[0]->plunked }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2015[0]->run }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2015[0]->earned_run }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2015[0]->ERA }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2015[0]->throw_full_innings }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2015[0]->win }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2015[0]->lose }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2015[0]->hold }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2015[0]->save }}</td>
                        </tr>
                        @endif
                        @if($pitching_data_2016[0]->games > 0)
                        <tr>
                            <td class="has-text-centered">2016</td>
                            <td class="has-text-centered">{{ $pitching_data_2016[0]->games }}</td>
                            <td class="has-text-centered">
                                {{ $pitching_data_2016[0]->innings }}
                                @if($pitching_data_2016[0]->fraction_innings > 0)
                                    {{ $pitching_data_2016[0]->fraction_innings }}/3
                                @endif</td>
                            <td class="has-text-centered">{{ $pitching_data_2016[0]->at_bat }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2016[0]->hits }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2016[0]->homerun }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2016[0]->strike_out }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2016[0]->walk }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2016[0]->plunked }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2016[0]->run }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2016[0]->earned_run }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2016[0]->ERA }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2016[0]->throw_full_innings }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2016[0]->win }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2016[0]->lose }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2016[0]->hold }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2016[0]->save }}</td>
                        </tr>
                        @endif
                        @if($pitching_data_2017[0]->games > 0)
                        <tr>
                            <td class="has-text-centered">2017</td>
                            <td class="has-text-centered">{{ $pitching_data_2017[0]->games }}</td>
                            <td class="has-text-centered">
                                {{ $pitching_data_2017[0]->innings }}
                                @if($pitching_data_2017[0]->fraction_innings > 0)
                                    {{ $pitching_data_2017[0]->fraction_innings }}/3
                                @endif</td>
                            <td class="has-text-centered">{{ $pitching_data_2017[0]->at_bat }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2017[0]->hits }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2017[0]->homerun }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2017[0]->strike_out }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2017[0]->walk }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2017[0]->plunked }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2017[0]->run }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2017[0]->earned_run }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2017[0]->ERA }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2017[0]->throw_full_innings }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2017[0]->win }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2017[0]->lose }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2017[0]->hold }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2017[0]->save }}</td>
                        </tr>
                        @endif
                        @if($pitching_data_2018[0]->games > 0)
                        <tr>
                            <td class="has-text-centered">2018</td>
                            <td class="has-text-centered">{{ $pitching_data_2018[0]->games }}</td>
                            <td class="has-text-centered">
                                {{ $pitching_data_2018[0]->innings }}
                                @if($pitching_data_2018[0]->fraction_innings > 0)
                                    {{ $pitching_data_2018[0]->fraction_innings }}/3
                                @endif</td>
                            <td class="has-text-centered">{{ $pitching_data_2018[0]->at_bat }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2018[0]->hits }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2018[0]->homerun }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2018[0]->strike_out }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2018[0]->walk }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2018[0]->plunked }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2018[0]->run }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2018[0]->earned_run }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2018[0]->ERA }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2018[0]->throw_full_innings }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2018[0]->win }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2018[0]->lose }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2018[0]->hold }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2018[0]->save }}</td>
                        </tr>
                        @endif
                        @if($pitching_data_2019[0]->games > 0)
                        <tr>
                            <td class="has-text-centered">2019</td>
                            <td class="has-text-centered">{{ $pitching_data_2019[0]->games }}</td>
                            <td class="has-text-centered">
                                {{ $pitching_data_2019[0]->innings }}
                                @if($pitching_data_2019[0]->fraction_innings > 0)
                                    {{ $pitching_data_2019[0]->fraction_innings }}/3
                                @endif</td>
                            <td class="has-text-centered">{{ $pitching_data_2019[0]->at_bat }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2019[0]->hits }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2019[0]->homerun }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2019[0]->strike_out }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2019[0]->walk }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2019[0]->plunked }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2019[0]->run }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2019[0]->earned_run }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2019[0]->ERA }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2019[0]->throw_full_innings }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2019[0]->win }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2019[0]->lose }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2019[0]->hold }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2019[0]->save }}</td>
                        </tr>
                        @endif
                        @if($pitching_data_2020[0]->games > 0)
                        <tr>
                            <td class="has-text-centered">2020</td>
                            <td class="has-text-centered">{{ $pitching_data_2020[0]->games }}</td>
                            <td class="has-text-centered">
                                {{ $pitching_data_2020[0]->innings }}
                                @if($pitching_data_2020[0]->fraction_innings > 0)
                                    {{ $pitching_data_2020[0]->fraction_innings }}/3
                                @endif</td>
                            <td class="has-text-centered">{{ $pitching_data_2020[0]->at_bat }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2020[0]->hits }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2020[0]->homerun }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2020[0]->strike_out }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2020[0]->walk }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2020[0]->plunked }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2020[0]->run }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2020[0]->earned_run }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2020[0]->ERA }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2020[0]->throw_full_innings }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2020[0]->win }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2020[0]->lose }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2020[0]->hold }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2020[0]->save }}</td>
                        </tr>
                        @endif
                        @if($pitching_data_2021[0]->games > 0)
                        <tr>
                            <td class="has-text-centered">2021</td>
                            <td class="has-text-centered">{{ $pitching_data_2021[0]->games }}</td>
                            <td class="has-text-centered">
                                {{ $pitching_data_2021[0]->innings }}
                                @if($pitching_data_2021[0]->fraction_innings > 0)
                                    {{ $pitching_data_2021[0]->fraction_innings }}/3
                                @endif</td>
                            <td class="has-text-centered">{{ $pitching_data_2021[0]->at_bat }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2021[0]->hits }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2021[0]->homerun }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2021[0]->strike_out }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2021[0]->walk }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2021[0]->plunked }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2021[0]->run }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2021[0]->earned_run }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2021[0]->ERA }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2021[0]->throw_full_innings }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2021[0]->win }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2021[0]->lose }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2021[0]->hold }}</td>
                            <td class="has-text-centered">{{ $pitching_data_2021[0]->save }}</td>
                        </tr>
                        @endif
                        @if($pitching_data_all[0]->games > 0)
                        <tr class="has-background-light has-text-weight-semibold">
                            <td class="has-text-centered">通算</td>
                            <td class="has-text-centered">{{ $pitching_data_all[0]->games }}</td>
                            <td class="has-text-centered">
                                {{ $pitching_data_all[0]->innings }}
                                @if($pitching_data_all[0]->fraction_innings > 0)
                                    {{ $pitching_data_all[0]->fraction_innings }}/3
                                @endif</td>
                            <td class="has-text-centered">{{ $pitching_data_all[0]->at_bat }}</td>
                            <td class="has-text-centered">{{ $pitching_data_all[0]->hits }}</td>
                            <td class="has-text-centered">{{ $pitching_data_all[0]->homerun }}</td>
                            <td class="has-text-centered">{{ $pitching_data_all[0]->strike_out }}</td>
                            <td class="has-text-centered">{{ $pitching_data_all[0]->walk }}</td>
                            <td class="has-text-centered">{{ $pitching_data_all[0]->plunked }}</td>
                            <td class="has-text-centered">{{ $pitching_data_all[0]->run }}</td>
                            <td class="has-text-centered">{{ $pitching_data_all[0]->earned_run }}</td>
                            <td class="has-text-centered">{{ $pitching_data_all[0]->ERA }}</td>
                            <td class="has-text-centered">{{ $pitching_data_all[0]->throw_full_innings }}</td>
                            <td class="has-text-centered">{{ $pitching_data_all[0]->win }}</td>
                            <td class="has-text-centered">{{ $pitching_data_all[0]->lose }}</td>
                            <td class="has-text-centered">{{ $pitching_data_all[0]->hold }}</td>
                            <td class="has-text-centered">{{ $pitching_data_all[0]->save }}</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection
