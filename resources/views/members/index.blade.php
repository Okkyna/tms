@extends('layout.base')

@section('title')
選手紹介｜大阪の草野球チームTwinbee
@endsection

@section('content')

<!-- 下層タイトル　-->
<div class="container is-fluid has-background-info pt-40 pb-40 mb-50">
    <div class="container">
        <h1 class="title is-2 has-text-white">選手紹介</h1>
        <p class="subtitle is-6 has-text-white">ツインビーに在籍する選手・スタッフをご覧いただけます。</p>
    </div>
</div>

<div class="container">
        <div class="columns is-marginless is-multiline">
            @foreach($members as $member)
            <div class="column is-6">
                <a href="{{ url('members', $member->player_id) }}">
                    <div class="columns is-mobile is-marginless has-background-info has-text-white is-vcentered">
                        <div class="column is-4 is-paddingless">
                            <img src="/img/members/{{ $member->photo_main }}">
                        </div>

                        <div class="column is-6 is-size-4 has-text-weight-bold">
                            <span class="">{{ $member->number }}</span>&nbsp;&nbsp;<span class="">{{ $member->name }}</span>
                        </div>
                        <div class="column is-2 has-text-right">
                            <span class="icon"><i class="fas fa-angle-right fa-2x"></i></span>
                        </div>

                    </div>
                </a>
            </div>
            @endforeach
        </div>
</div>

@endsection
