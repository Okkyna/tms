@extends('layout.base')

@section('title')
スケジュール｜大阪の草野球チーム Twinbee
@endsection

@section('content')


<!-- 下層タイトル　-->
<div class="container is-fluid has-background-info pt-40 pb-40 mb-50">
    <div class="container">
        <h1 class="title is-2 has-text-white">スケジュール</h1>
        <p class="subtitle is-6 has-text-white">本日以降の活動予定をご覧いただけます。対戦相手が未決定の日程は、随時対戦相手様を募集しております。</p>
    </div>
</div>

<div class="container">
    <div class="columns is-multiline">
        @foreach($schedule as $s)
        <div class="column is-half">
            <div class="schedule_panel has-background-dark has-text-white">
                @if($s->name)
                <div class="date has-text-centered ">{{ Carbon\Carbon::parse($s->date)->format('Y/m/d') }}&nbsp;&nbsp;{{ Carbon\Carbon::parse($s->start_time)->format('H:i') }}〜{{ Carbon\Carbon::parse($s->end_time)->format('H:i') }}</div>
                <div class="ground has-text-centered ">{{ $s->name }}</div>
                @else
                <div class="date has-text-centered ">{{ Carbon\Carbon::parse($s->date)->format('Y/m/d') }}</div>
                <div class="ground has-text-centered ">グランド未確保</div>
                @endif
                @if($s->opponent)
                <div class="opponent is-size-4 has-text-weight-bold has-text-centered">{{ $s->opponent }}様戦</div>
                @else
                <div class="opponent is-size-4 has-text-weight-bold has-text-centered">対戦相手様募集中</div>
                @endif
            </div>
        </div>
        @endforeach
    </div>
</div>


@endsection
